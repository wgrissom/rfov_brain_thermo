# rFOV_brain_thermo

This repository contains data and source code to process and generate all the figures in the paper 'Reducing temperature errors in transcranial MR-guided focused ultrasound using a reduced-field-of-view excitation', which was submitted to Magnetic Resonance in Medicine for consideration as a Note in April 2019. 

Running the processing scripts requires that you have the kspace_hybrid_thermo function in your MATLAB path. This code can be downloaded from [here](https://bitbucket.org/wgrissom/k-space-thermometry/).