% load axial images and temp maps
ax = load('axial_results');
% load coronal images and temp maps
co = load('coronal_results');
% load sagittal images and temp maps
sa = load('sagittal_results');


% now make a movie of the errors and images over time
% state what time point each image is at
figure;
set(gcf,'color',[0 0 0],'position',[18 82 1634 389]);
F(28) = struct('cdata',[],'colormap',[]);
baseInd = 2;
for ii = 1:28
    
    % axial
    
    % first row: magnitude images
    % second row: temp std maps
    magImgs = abs([ax.imgFull(:,:,ii+baseInd) ax.imgrFOV(:,:,ii+baseInd)]);
    magImgs = magImgs./1000;magImgs(magImgs > 1000) = 1;
    magImgs = repmat(magImgs,[1 1 3]);
    tempMaps = [ax.mask.*ax.tempFull(:,:,ii) ax.mask.*ax.temprFOV(:,:,ii)];
    threeColor = zeros([size(tempMaps) 3]);
    map = parula(64); % get a colormap
    % interpolate temp errors onto colormap
    tempMaps(tempMaps > 1) = 0.5;
    tempMaps(tempMaps < -1) = -0.5;
    for kk = 1:3 % loop over color channels
        threeColor(:,:,kk) = reshape(interp1(linspace(-0.5,0.5,64),map(:,kk),tempMaps(:)),size(tempMaps));
    end
    threeColor = threeColor.*repmat(ax.mask,[1 2 3]);
    %figure;imagesc(threeColor);
    %axis image;axis off
    
    bigImg = cat(1,magImgs,threeColor);
    
    fontColor = [0.75 0.75 0.75];
    clf;
    subplot(131)
    imagesc(bigImg);
    axis image;axis off
    
    h = text(128,-25,'Full-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    h = text(128+256,-25,'Reduced-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    h = text(0,256,'Axial','Rotation',90,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',ax.t_Full(ii));
    h = text(128,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',ax.t_rFOV(ii));
    h = text(128+256,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    
    
    % move to the far left
    pos = get(gca,'position');
    set(gca,'position',[0.0 pos(2:end)]);
    
    h = colorbar;
    set(h,'Limits',[-0.5 0.5],'Ticks',[-0.5 -0.25 0 0.25 0.5],'TickLabels',{'-0.5','-0.25','0','0.25','0.5'},'FontSize',14,'Color',fontColor);
    caxis([-0.5 0.5]);
    pos = get(h,'position');
    set(h,'position',[pos(1:3) 0.375]);
    h = text(550,384,'^{\circ}C','FontSize',20,'Color',fontColor);
    
    
    % coronal
    
    % first row: magnitude images
    % second row: temp std maps
    magImgs = abs([co.imgFull(:,:,ii+baseInd) co.imgrFOV(:,:,ii+baseInd)]);
    magImgs = magImgs./1000;magImgs(magImgs > 1000) = 1;
    magImgs = repmat(magImgs,[1 1 3]);
    tempMaps = [co.mask.*co.tempFull(:,:,ii) co.mask.*co.temprFOV(:,:,ii)];
    threeColor = zeros([size(tempMaps) 3]);
    map = parula(64); % get a colormap
    % interpolate temp errors onto colormap
    tempMaps(tempMaps > 1) = 1;
    tempMaps(tempMaps < -1) = -1;
    for kk = 1:3 % loop over color channels
        threeColor(:,:,kk) = reshape(interp1(linspace(-1,1,64),map(:,kk),tempMaps(:)),size(tempMaps));
    end
    threeColor = threeColor.*repmat(co.mask,[1 2 3]);
    %figure;imagesc(threeColor);
    %axis image;axis off
    
    bigImg = cat(1,magImgs,threeColor);
    
    subplot(132)
    imagesc(bigImg);
    axis image;axis off
    
    h = text(128,-25,'Full-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    h = text(128+256,-25,'Reduced-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    h = text(0,256,'Coronal','Rotation',90,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',co.t_Full(ii));
    h = text(128,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',co.t_rFOV(ii));
    h = text(128+256,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);

    % move to the far left
    pos = get(gca,'position');
    set(gca,'position',[0.24 pos(2:end)]);
    
    h = colorbar;
    set(h,'Limits',[-1 1],'Ticks',[-1 -0.5 0 0.5 1],'TickLabels',{'-1','-0.5','0','0.5','1'},'FontSize',14,'Color',fontColor);
    caxis([-1 1]);
    pos = get(h,'position');
    set(h,'position',[pos(1:3) 0.375]);
    h = text(550,384,'^{\circ}C','FontSize',20,'Color',fontColor);
   
    
    % sagittal
    
    % first row: magnitude images
    % second row: temp std maps
    magImgs = abs([sa.imgFull(:,:,ii+baseInd) sa.imgrFOV(:,:,ii+baseInd)]);
    magImgs = magImgs./1000;magImgs(magImgs > 1000) = 1;
    magImgs = repmat(magImgs,[1 1 3]);
    tempMaps = [sa.mask.*sa.tempFull(:,:,ii) sa.mask.*sa.temprFOV(:,:,ii)];
    threeColor = zeros([size(tempMaps) 3]);
    map = parula(64); % get a colormap
    % interpolate temp errors onto colormap
    tempMaps(tempMaps > 1) = 1;
    tempMaps(tempMaps < -1) = -1;
    for kk = 1:3 % loop over color channels
        threeColor(:,:,kk) = reshape(interp1(linspace(-1,1,64),map(:,kk),tempMaps(:)),size(tempMaps));
    end
    threeColor = threeColor.*repmat(sa.mask,[1 2 3]);
    %figure;imagesc(threeColor);
    %axis image;axis off
    
    bigImg = cat(1,magImgs,threeColor);
    
    subplot(133)
    imagesc(bigImg);
    axis image;axis off
    
    h = text(128,-25,'Full-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    h = text(128+256,-25,'Reduced-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    h = text(0,256,'Sagittal','Rotation',90,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',sa.t_Full(ii));
    h = text(128,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',sa.t_rFOV(ii));
    h = text(128+256,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);

    % move to the far left
    pos = get(gca,'position');
    set(gca,'position',[0.48 pos(2:end)]);
    
    h = colorbar;
    set(h,'Limits',[-1 1],'Ticks',[-1 -0.5 0 0.5 1],'TickLabels',{'-1','-0.5','0','0.5','1'},'FontSize',14,'Color',fontColor);
    caxis([-1 1]);
    pos = get(h,'position');
    set(h,'position',[pos(1:3) 0.375]);
    h = text(550,384,'^{\circ}C','FontSize',20,'Color',fontColor);
    
    drawnow;
    F(ii) = getframe(gcf);
    F(ii).cdata = F(ii).cdata(:,1:2350,:);
    
end

v = VideoWriter('inVivoImagesAndTempErrors.mp4','MPEG-4');
v.FrameRate = 4;
v.Quality = 95;
open(v)
writeVideo(v,F)
close(v)
