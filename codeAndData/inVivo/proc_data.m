imgNumsRe = (0:29)*4 + 3;
imgNumsIm = (0:29)*4 + 4;

dataSelect = 'sagittal';
switch dataSelect
    case 'axial'
        folderFull = 'AxialInVivo_Full_57';
        folderrFOV = 'AxialInVivo_rFOV_56';
        load axialMask;
        dispMaxStd = 0.25;
        dispMaxMean = 0.25;
    case 'coronal'
        folderFull = 'CoronalInVivo_Full_60';
        folderrFOV = 'CoronalInVivo_rFOV_59';
        load coronalMask;
        dispMaxStd = 0.25;
        dispMaxMean = 0.25;
    case 'sagittal'
        folderFull = 'SagittalInVivo_Full_66';
        folderrFOV = 'SagittalInVivo_rFOV_65';
        load sagittalMask;
        dispMaxStd = 0.25;
        dispMaxMean = 0.25;
end

% load full-EX data
for ii = 1:30
    imgFull(:,:,ii) = double(dicomread([folderFull '/IM-0002-' sprintf('%04d',imgNumsRe(ii))]));
    imgFull(:,:,ii) = imgFull(:,:,ii) + ...
        1i * double(dicomread([folderFull '/IM-0002-' sprintf('%04d',imgNumsIm(ii))]));
end

% load rFOV data
for ii = 1:30
    imgrFOV(:,:,ii) = double(dicomread([folderrFOV '/IM-0001-' sprintf('%04d',imgNumsRe(ii))]));
    imgrFOV(:,:,ii) = imgrFOV(:,:,ii) + ...
        1i * double(dicomread([folderrFOV '/IM-0001-' sprintf('%04d',imgNumsIm(ii))]));
end

teFull = 13; % ms
terFOV = 17.3; % ms
cFull = 1/(3*2*pi*42.58*0.01*teFull/1000);
crFOV = 1/(3*2*pi*42.58*0.01*terFOV/1000);

% [x,y] = meshgrid(1:256);

% % load phantom masks
% if ~exist('axialMask.mat','file')
%     addpath ~/code/uva_fusf_rthawk/inccaGradData
%     mask = roiSelect(abs(imgFull(:,:,1))+abs(imgrFOV(:,:,1)));
%     % also thresholded to 0.15 of max amplitude of imgrFOV
%     save axialMask.mat mask
% else
%     load axialMask.mat
% end

% calculate temperature std through-time and within phantom mask for each
% time point and for whole time-series
baseInd = 1;
%tempFull = cFull * angle(imgFull(:,:,baseInd + 1:end).*conj(repmat(imgFull(:,:,baseInd),[1 1 30-baseInd])));
%temprFOV = crFOV * angle(imgrFOV(:,:,baseInd + 1:end).*conj(repmat(imgrFOV(:,:,baseInd),[1 1 30-baseInd])));

N  = 256;

parfor ii = baseInd+1:size(imgFull,3)
    
    printf('working on dynamic %d',ii);
    
    acqp = struct('k',[]); % image domain
    acqp.mask = true(N);
    acqp.fov = 1;
    algp = struct('dofigs',1);
    algp.order = 0; % polynomial order
    algp.modeltest = true;
    algp.beta = -1; % no spatial regularization
    
    thetainit = zeros(N,N); % initialize temp phase shift map with zeros
    
    % full FOV
    acqp.data = mask.*imgFull(:,:,ii); % heating image
    acqp.data = acqp.data(:);
    acqp.L = mask.*imgFull(:,:,baseInd);acqp.L = acqp.L(:); % baseline
    [thetakacc,A,c] = kspace_hybrid_thermo(acqp,thetainit,algp);
    Ac = reshape(A*c,[N N]);
    tempFull(:,:,ii) = cFull*real(thetakacc);
    %tempFull(:,:,ii) = cFull*angle(imgFull(:,:,ii).*conj(exp(1i*Ac).*imgFull(:,:,baseInd)));
    
    % reduced FOV
    acqp.data = mask.*imgrFOV(:,:,ii); % heating image
    acqp.data = acqp.data(:);
    acqp.L = mask.*imgrFOV(:,:,baseInd);acqp.L = acqp.L(:); % baseline
    [thetakacc,A,c] = kspace_hybrid_thermo(acqp,thetainit,algp);
    Ac = reshape(A*c,[N N]);
    temprFOV(:,:,ii) = crFOV*real(thetakacc);
    %temprFOV(:,:,ii) = crFOV*angle(imgrFOV(:,:,ii).*conj(exp(1i*Ac).*imgrFOV(:,:,baseInd)));
    
end
tempFull = tempFull(:,:,baseInd+1:end);
%tempFull = repmat(mask,[1 1 size(tempFull,3)]).*tempFull;
temprFOV = temprFOV(:,:,baseInd+1:end);
%temprFOV = repmat(mask,[1 1 size(temprFOV,3)]).*temprFOV;

% % mean-correct each time point
% for ii = 1:size(tempFull,3)
%     tmp = tempFull(:,:,ii);
%     meanVal = mean(tmp(mask));
%     tempFull(:,:,ii) = tempFull(:,:,ii) - meanVal;
% end
% 
% % mean-correct each time point
% for ii = 1:size(temprFOV,3)
%     tmp = temprFOV(:,:,ii);
%     meanVal = mean(tmp(mask));
%     temprFOV(:,:,ii) = temprFOV(:,:,ii) - meanVal;
% end

% 0.18 and 0.12 for axial, with baseInd = 1, order = 0
% 0.32 and 0.19 for coronal, with baseInd = 1, order = 0
% 0.44 and 0.21 for sagittal, with baseInd = 1, order = 0
% means over time and space were all smaller than 0.01 deg C
maskStdCal = tempFull ~= 0 & temprFOV ~= 0;maskStdCal = logical(prod(maskStdCal,3));
tempStdFull = std(tempFull(repmat(maskStdCal,[1 1 size(tempFull,3)])))
tempStdrFOV = std(temprFOV(repmat(maskStdCal,[1 1 size(temprFOV,3)])))
tempMeanFull = mean(tempFull(repmat(maskStdCal,[1 1 size(tempFull,3)])))
tempMeanrFOV = mean(temprFOV(repmat(maskStdCal,[1 1 size(temprFOV,3)])))

% figure;
% subplot(121)
% imagesc(mask.*std(tempFull,[],3),[0 1]);axis image
% title 'Full'
% subplot(122)
% imagesc(mask.*std(temprFOV,[],3),[0 1]);axis image
% title 'rFOV'

% what is expected steady state signal for each? 
sFull = sind(30)*(1-exp(-26/1000))/(1-cosd(30)*exp(-26/1000))*exp(-13/45);
srFOV = sind(30)*(1-exp(-44/1000))/(1-cosd(30)*exp(-44/1000))*exp(-17/45);
% ratio is 1.4 which is about right - the rFOV signal is about 30% higher

% first row: magnitude images
% second row: temp std maps
% third row: temp mean maps
magImgs = abs([imgFull(:,:,baseInd) imgrFOV(:,:,baseInd)]);
magImgs = magImgs./1000;magImgs(magImgs > 1000) = 1;
magImgs = repmat(magImgs,[1 1 3]);
% REDFLAG: Need to calculate through-time std only for voxels that 
% have non-zero temps. 
stdMaps = zeros(N,2*N);
for ii = 1:N
    for jj = 1:N
        if mask(ii,jj)
            tmp = squeeze(tempFull(ii,jj,:));
            stdMaps(ii,jj) = std(tmp(tmp ~= 0));
            tmp = squeeze(temprFOV(ii,jj,:));
            stdMaps(ii,jj+N) = std(tmp(tmp ~= 0));
        end
    end
end
meanMaps = zeros(N,2*N);
for ii = 1:N
    for jj = 1:N
        if mask(ii,jj)
            tmp = squeeze(tempFull(ii,jj,:));
            meanMaps(ii,jj) = mean(tmp(tmp ~= 0));
            tmp = squeeze(temprFOV(ii,jj,:));
            meanMaps(ii,jj+N) = mean(tmp(tmp ~= 0));
        end
    end
end
%stdMaps = [mask.*std(tempFull,[],3) mask.*std(temprFOV,[],3)];
threeColor = zeros([size(stdMaps) 3]);
map = parula(64); % get a colormap
% interpolate temp errors onto colormap
stdMaps(stdMaps > dispMaxStd) = dispMaxStd;
for kk = 1:3 % loop over color channels
  threeColor(:,:,kk) = reshape(interp1(linspace(0,dispMaxStd,64),map(:,kk),stdMaps(:)),size(stdMaps));
end
threeColor = threeColor.*repmat(mask,[1 2 3]);
%figure;imagesc(threeColor);
%axis image;axis off

threeColorMean = zeros([size(meanMaps) 3]);
map = parula(64); % get a colormap
% interpolate temp errors onto colormap
meanMaps(meanMaps > dispMaxMean) = dispMaxMean;
meanMaps(meanMaps < -dispMaxMean) = -dispMaxMean;
for kk = 1:3 % loop over color channels
  threeColorMean(:,:,kk) = reshape(interp1(linspace(-dispMaxMean,dispMaxMean,64),map(:,kk),meanMaps(:)),size(meanMaps));
end
threeColorMean = threeColorMean.*repmat(mask,[1 2 3]);
%figure;imagesc(threeColor);
%axis image;axis off

bigImg = cat(1,magImgs,threeColor,threeColorMean);
figure;imagesc(bigImg);
axis image;axis off

h = colorbar; 
fontColor = [0.75 0.75 0.75];
set(h,'Limits',[0 dispMaxStd],'Ticks',[0 dispMaxStd/2 dispMaxStd],...
    'TickLabels',{'0',num2str(dispMaxStd/2),num2str(dispMaxStd)},...
    'FontSize',14,'Color',fontColor);
caxis([0 dispMaxStd]);
pos = get(h,'position');
set(h,'position',[0.83 pos(2:3) 0.375]);
h = text(585,392,'^{\circ}C','HorizontalAlignment','Left','FontSize',20,'Color',fontColor);

% save images and parameters for making a movie
tr_Full = 25.7;
tr_rFOV = 43.9;
t_rFOV = (0:27)*tr_rFOV/1000*128*0.6;
t_Full = (0:27)*tr_Full/1000*128;

save([dataSelect '_results'],'t_rFOV','t_Full','imgFull','imgrFOV','mask','tempFull','temprFOV');





