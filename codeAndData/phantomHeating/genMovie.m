% load axial images and temp maps
ax = load('axial_results');
% load coronal images and temp maps
co = load('coronal_results');
% load sagittal images and temp maps
sa = load('sagittal_results');


% now make a movie of the errors and images over time
% state what time point each image is at
figure;
set(gcf,'color',[0 0 0],'position',[18 79 640 400]);
F(28) = struct('cdata',[],'colormap',[]);
baseInd = 2;
for ii = 1:28
    
    % axial
    
    % first row: magnitude images
    % second row: temp map, unwindowed
    % third row: temp map, windowed
    magImgs = abs([ax.imgFull(:,:,ii+baseInd) ax.imgrFOV(:,:,ii+baseInd)]);
    magImgs = magImgs./4500;magImgs(magImgs > 4500) = 1;
    magImgs = repmat(magImgs,[1 1 3]);
    
    % temp maps - unwindowed
    tempMaps = [ax.tempFull(ax.yInds,ax.xInds,ii) ax.temprFOV(ax.yInds,ax.xInds,ii)];
    tempMaps(tempMaps > 14) = 14;
    tempMaps(tempMaps < -2) = -2;
    % temp Maps - windowed
    tempMapsw = [ax.tempFull(ax.yInds,ax.xInds,ii) ax.temprFOV(ax.yInds,ax.xInds,ii)];
    tempMapsw(tempMapsw > 3) = 3;
    tempMapsw(tempMapsw < -3) = -3;

    threeColor = zeros([size(tempMaps) 3]);
    map = parula(64); % get a colormap
    % interpolate temp errors onto colormap
    for kk = 1:3 % loop over color channels
        threeColor(:,:,kk) = reshape(interp1(linspace(-2,14,64),map(:,kk),tempMaps(:)),size(tempMaps));
    end
    
    threeColorw = zeros([size(tempMaps) 3]);
    map = parula(64); % get a colormap
    % interpolate temp errors onto colormap
    for kk = 1:3 % loop over color channels
        threeColorw(:,:,kk) = reshape(interp1(linspace(-3,3,64),map(:,kk),tempMapsw(:)),size(tempMapsw));
    end

    
    subplot(321)
    imagesc(magImgs);axis image
    axis off
    h=colorbar;
    set(h,'visible','off');
    fontColor = [0.75 0.75 0.75];
    h = text(128,-25,'Full-FOV','HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    h = text(128+256,-25,'Reduced-FOV','HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    
    % move to the far left
    pos = get(gca,'position');
    set(gca,'position',[pos(1) 0.65 pos(3:end)]);
    
    
    subplot(323)
    imagesc(threeColor);axis image
    axis off
    h = colorbar;
    fontColor = [0.75 0.75 0.75];
    set(h,'Limits',[-2 14],'Ticks',[-2 2 6 10 14],...
        'TickLabels',{'-2','2','6','10','14'},'FontSize',10,'Color',fontColor);
    caxis([-2 14]);
    h = text(156,32,'\Delta^{\circ}C','FontSize',12,'Color',fontColor);
    
    % pos = get(h,'position');
    % set(h,'position',[0.83 pos(2:3) 0.375]);
    % h = text(585,392,'^{\circ}C','HorizontalAlignment','Left','FontSize',20,'Color',fontColor);
    h = text(-8,32,'Axial','Rotation',90,'HorizontalAlignment','Center','FontSize',16,'FontWeight','bold','Color',fontColor);
        
    % move to the far left
    pos = get(gca,'position');
    set(gca,'position',[pos(1) pos(2:end)]);
    
    subplot(325)
    imagesc(threeColorw);axis image
    axis off
    h = colorbar;
    fontColor = [0.75 0.75 0.75];
    set(h,'Limits',[-3 3],'Ticks',-3:1.5:3,...
        'TickLabels',{'-3','-1.5','0','1.5','3'},'FontSize',10,'Color',fontColor);
    caxis([-3 3]);
    h = text(156,32,'\Delta^{\circ}C','FontSize',12,'Color',fontColor);
    
        
    timeStr = sprintf('%0.1fs',ax.t_Full(ii));
    h = text(32,72,timeStr,'HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',ax.t_rFOV(ii));
    h = text(32+64,72,timeStr,'HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    
    
    
    % move to the far left
    pos = get(gca,'position');
    set(gca,'position',[pos(1) 0.16 pos(3:end)]);
    %set(gca,'position',[0.0 pos(2:end)]);
    
%     h = colorbar;
%     set(h,'Limits',[-2 2],'Ticks',[-2 -1 0 1 2],'TickLabels',{'-2','-1','0','1','2'},'FontSize',14,'Color',fontColor);
%     caxis([-2 2]);
%     pos = get(h,'position');
%     set(h,'position',[pos(1:3) 0.375]);
%     h = text(550,384,'^{\circ}C','FontSize',20,'Color',fontColor);
    
    
%     % coronal
%     
%     % first row: magnitude images
%     % second row: temp std maps
%     magImgs = abs([co.imgFull(:,:,ii+baseInd) co.imgrFOV(:,:,ii+baseInd)]);
%     magImgs = magImgs./1000;magImgs(magImgs > 1000) = 1;
%     magImgs = repmat(magImgs,[1 1 3]);
%     tempMaps = [co.mask.*co.tempFull(:,:,ii) co.mask.*co.temprFOV(:,:,ii)];
%     threeColor = zeros([size(tempMaps) 3]);
%     map = parula(64); % get a colormap
%     % interpolate temp errors onto colormap
%     tempMaps(tempMaps > 2) = 2;
%     tempMaps(tempMaps < -2) = -2;
%     for kk = 1:3 % loop over color channels
%         threeColor(:,:,kk) = reshape(interp1(linspace(-2,2,64),map(:,kk),tempMaps(:)),size(tempMaps));
%     end
%     threeColor = threeColor.*repmat(co.mask,[1 2 3]);
%     %figure;imagesc(threeColor);
%     %axis image;axis off
%     
%     bigImg = cat(1,magImgs,threeColor);
%     
%     subplot(132)
%     imagesc(bigImg);
%     axis image;axis off
%     
%     h = text(128,-25,'Full-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
%     h = text(128+256,-25,'Reduced-FOV','HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
%     h = text(0,256,'Coronal','Rotation',90,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
%     timeStr = sprintf('%0.1fs',co.t_Full(ii));
%     h = text(128,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
%     timeStr = sprintf('%0.1fs',co.t_rFOV(ii));
%     h = text(128+256,512,timeStr,'HorizontalAlignment','Center','FontSize',20,'FontWeight','bold','Color',fontColor);
% 
%     % move to the far left
%     pos = get(gca,'position');
%     set(gca,'position',[0.24 pos(2:end)]);
%     
%     h = colorbar;
%     set(h,'Limits',[-2 2],'Ticks',[-2 -1 0 1 2],'TickLabels',{'-2','-1','0','1','2'},'FontSize',14,'Color',fontColor);
%     caxis([-2 2]);
%     pos = get(h,'position');
%     set(h,'position',[pos(1:3) 0.375]);
%     h = text(550,384,'^{\circ}C','FontSize',20,'Color',fontColor);
   
    
    % sagittal
    
    % first row: magnitude images
    % second row: temp map, unwindowed
    % third row: temp map, windowed
    magImgs = abs([sa.imgFull(:,:,ii+baseInd) sa.imgrFOV(:,:,ii+baseInd)]);
    magImgs = magImgs./4500;magImgs(magImgs > 4500) = 1;
    magImgs = repmat(magImgs,[1 1 3]);
    
    % temp maps - unwindowed
    tempMaps = [sa.tempFull(sa.yInds,sa.xInds,ii) sa.temprFOV(sa.yInds,sa.xInds,ii)];
    tempMaps(tempMaps > 10) = 10;
    tempMaps(tempMaps < -2) = -2;
    % temp Maps - windowed
    tempMapsw = [sa.tempFull(sa.yInds,sa.xInds,ii) sa.temprFOV(sa.yInds,sa.xInds,ii)];
    tempMapsw(tempMapsw > 2) = 2;
    tempMapsw(tempMapsw < -2) = -2;

    threeColor = zeros([size(tempMaps) 3]);
    map = parula(64); % get a colormap
    % interpolate temp errors onto colormap
    for kk = 1:3 % loop over color channels
        threeColor(:,:,kk) = reshape(interp1(linspace(-2,10,64),map(:,kk),tempMaps(:)),size(tempMaps));
    end
    
    threeColorw = zeros([size(tempMaps) 3]);
    map = parula(64); % get a colormap
    % interpolate temp errors onto colormap
    for kk = 1:3 % loop over color channels
        threeColorw(:,:,kk) = reshape(interp1(linspace(-2,2,64),map(:,kk),tempMapsw(:)),size(tempMapsw));
    end

    
    subplot(322)
    imagesc(magImgs);axis image
    axis off
    h=colorbar;
    set(h,'visible','off');
    fontColor = [0.75 0.75 0.75];
    h = text(128,-25,'Full-FOV','HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    h = text(128+256,-25,'Reduced-FOV','HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    
    % move to the far left
    pos = get(gca,'position');
        set(gca,'position',[pos(1) 0.65 pos(3:end)]);
    %set(gca,'position',[0.48 pos(2:end)]);
    
    subplot(324)
    imagesc(threeColor);axis image
    axis off
    h = colorbar; 
    fontColor = [0.75 0.75 0.75];
    set(h,'Limits',[-2 10],'Ticks',[-2 2 6 10],...
        'TickLabels',{'-2','2','6','10'},'FontSize',10,'Color',fontColor);
    caxis([-2 10]);
    h = text(156,32,'\Delta^{\circ}C','FontSize',12,'Color',fontColor);
    
    % pos = get(h,'position');
    % set(h,'position',[0.83 pos(2:3) 0.375]);
    % h = text(585,392,'^{\circ}C','HorizontalAlignment','Left','FontSize',20,'Color',fontColor);
    h = text(-8,32,'Sagittal','Rotation',90,'HorizontalAlignment','Center','FontSize',16,'FontWeight','bold','Color',fontColor);
        
    % move to the far left
    pos = get(gca,'position');
    %pos = get(gca,'position');
    set(gca,'position',[pos(1) pos(2:end)]);
    
    subplot(326)
    imagesc(threeColorw);axis image
    axis off
    h = colorbar; 
    fontColor = [0.75 0.75 0.75];
    set(h,'Limits',[-2 2],'Ticks',-2:2,...
        'TickLabels',{'-2','-1','0','1','2'},'FontSize',10,'Color',fontColor);
    caxis([-2 2]);
    h = text(156,32,'\Delta^{\circ}C','FontSize',12,'Color',fontColor);
    
        
    timeStr = sprintf('%0.1fs',sa.t_Full(ii));
    h = text(32,72,timeStr,'HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    timeStr = sprintf('%0.1fs',sa.t_rFOV(ii));
    h = text(32+64,72,timeStr,'HorizontalAlignment','Center','FontSize',12,'FontWeight','bold','Color',fontColor);
    
    
    % move to the far left
    pos = get(gca,'position');
        set(gca,'position',[pos(1) 0.16 pos(3:end)]);
    %set(gca,'position',[0.48 pos(2:end)]);
    
    
%     h = colorbar;
%     set(h,'Limits',[-2 2],'Ticks',[-2 -1 0 1 2],'TickLabels',{'-2','-1','0','1','2'},'FontSize',10,'Color',fontColor);
%     caxis([-2 2]);
%     pos = get(h,'position');
     %set(h,'position',[pos(1:3) 0.375]);
     
    drawnow;
    F(ii) = getframe(gcf);
    F(ii).cdata = F(ii).cdata(66:716,170:1244,:);
    
end

v = VideoWriter('phantomImagesAndTemps.mp4','MPEG-4');
v.FrameRate = 4;
v.Quality = 95;
open(v)
writeVideo(v,F)
close(v)
