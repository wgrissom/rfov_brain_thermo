
baseInd = 2;

useksh = true; % true for multibaseline study, false otherwise

imgNumsRe = (0:29)*4 + 3;
imgNumsIm = (0:29)*4 + 4;

% load full-EX data
for ii = 1:30
    imgFull(:,:,ii) = double(dicomread(['Axialfull30NoHeat_84/IM-0003-' sprintf('%04d',imgNumsRe(ii))]));
    imgFull(:,:,ii) = imgFull(:,:,ii) + ...
        1i * double(dicomread(['Axialfull30NoHeat_84/IM-0003-' sprintf('%04d',imgNumsIm(ii))]));
end

% load rFOV data
for ii = 1:30
    imgrFOV(:,:,ii) = double(dicomread(['AxialrFOV30NoHeat_82/IM-0001-' sprintf('%04d',imgNumsRe(ii))]));
    imgrFOV(:,:,ii) = imgrFOV(:,:,ii) + ...
        1i * double(dicomread(['AxialrFOV30NoHeat_82/IM-0001-' sprintf('%04d',imgNumsIm(ii))]));
end

teFull = 13; % ms
terFOV = 17.3; % ms
cFull = -1/(3*2*pi*42.58*0.01*teFull/1000);
crFOV = -1/(3*2*pi*42.58*0.01*terFOV/1000);

[x,y] = meshgrid(1:256);

% load phantom masks
if ~exist('axialPhantomMask.mat','file')
    addpath ~/code/uva_fusf_rthawk/inccaGradData
    mask = roiSelect(abs(imgFull(:,:,1))+abs(imgrFOV(:,:,1)));
    save axialPhantomMask.mat mask
else
    load axialPhantomMask.mat
end

maskXMean = mean(x(mask(:)));
maskYMean = mean(y(mask(:)));
mask = (x-round(maskXMean)).^2 + (y-round(maskYMean)-1).^2 <= 31^2; % radius 31

xInds = (-32:31) + round(maskXMean);
yInds = (-32:31) + round(maskYMean);

% calculate temperature std through-time and within phantom mask for each
% time point and for whole time-series

if useksh
    N  = 256;
    
    parfor ii = baseInd+1:size(imgFull,3)
        
        printf('working on dynamic %d',ii);
        
        acqp = struct('k',[]); % image domain
        acqp.mask = true(N);
        acqp.fov = 1;
        algp = struct('dofigs',1);
        algp.order = 0; % polynomial order
        algp.modeltest = true;
        algp.beta = -1; % no spatial regularization
        acqp.thetaSign = 1;
        %algp.lam = [10^-8 10^-8];
        
        thetainit = zeros(N,N); % initialize temp phase shift map with zeros
        
        % full FOV
        acqp.data = mask.*imgFull(:,:,ii); % heating image
        acqp.data = acqp.data(:);
        acqp.L = mask.*imgFull(:,:,baseInd);acqp.L = acqp.L(:); % baseline
        [thetakacc,A,c] = kspace_hybrid_thermo(acqp,thetainit,algp);
        Ac = reshape(A*c,[N N]);
        tempFull(:,:,ii) = cFull*real(thetakacc);
        %tempFull(:,:,ii) = cFull*angle(imgFull(:,:,ii).*conj(exp(1i*Ac).*imgFull(:,:,baseInd)));
        
        % reduced FOV
        acqp.data = mask.*imgrFOV(:,:,ii); % heating image
        acqp.data = acqp.data(:);
        acqp.L = mask.*imgrFOV(:,:,baseInd);acqp.L = acqp.L(:); % baseline
        [thetakacc,A,c] = kspace_hybrid_thermo(acqp,thetainit,algp);
        Ac = reshape(A*c,[N N]);
        temprFOV(:,:,ii) = crFOV*real(thetakacc);
        %temprFOV(:,:,ii) = crFOV*angle(imgrFOV(:,:,ii).*conj(exp(1i*Ac).*imgrFOV(:,:,baseInd)));
        
    end
    tempFull = tempFull(:,:,baseInd+1:end);
    %tempFull = repmat(mask,[1 1 size(tempFull,3)]).*tempFull;
    temprFOV = temprFOV(:,:,baseInd+1:end);
    %temprFOV = repmat(mask,[1 1 size(temprFOV,3)]).*temprFOV;
    
else
    
    tempFull = cFull * angle(imgFull(:,:,baseInd + 1:end).*conj(repmat(imgFull(:,:,baseInd),[1 1 30-baseInd])));
    temprFOV = crFOV * angle(imgrFOV(:,:,baseInd + 1:end).*conj(repmat(imgrFOV(:,:,baseInd),[1 1 30-baseInd])));
    
    % mean-correct each time point
    for ii = 1:size(tempFull,3)
        tmp = tempFull(:,:,ii);
        meanVal = mean(tmp(mask));
        tempFull(:,:,ii) = tempFull(:,:,ii) - meanVal;
    end
    
    % mean-correct each time point
    for ii = 1:size(temprFOV,3)
        tmp = temprFOV(:,:,ii);
        meanVal = mean(tmp(mask));
        temprFOV(:,:,ii) = temprFOV(:,:,ii) - meanVal;
    end
    
end


tempStdFull = std(tempFull(repmat(mask,[1 1 30-baseInd])))
tempStdrFOV = std(temprFOV(repmat(mask,[1 1 30-baseInd])))
tempMeanFull = mean(tempFull(repmat(mask,[1 1 30-baseInd])))
tempMeanrFOV = mean(temprFOV(repmat(mask,[1 1 30-baseInd])))

figure;
subplot(221)
imagesc(std(tempFull(yInds,xInds,:),[],3),[0 0.5]);axis image;axis off;colorbar
title 'Full'
subplot(222)
imagesc(std(temprFOV(yInds,xInds,:),[],3),[0 0.5]);axis image;axis off;colorbar
title 'rFOV'
subplot(223)
imagesc(mean(tempFull(yInds,xInds,:),3),[-1 1]);axis image;axis off;colorbar
title 'Full'
subplot(224)
imagesc(mean(temprFOV(yInds,xInds,:),3),[-1 1]);axis image;axis off;colorbar
title 'rFOV'


% hot spot at 131 95
tr_rFOV = 43.892; 
yFrac = 0.65;
t_rFOV = (0:size(tempFull,3)-1)*128*yFrac*tr_rFOV/1000;
tr_full = 25.696;
t_full = (0:size(tempFull,3)-1)*128*tr_full/1000;
figure;
plot(t_full,squeeze(tempFull(95,131,:)));
hold on
plot(t_rFOV,squeeze(temprFOV(95,131,:)));
axis([0 90 -2 2])
axis square
set(gcf,'position',[801 584 469 241]);
set(gca,'xtick',[0 15 30 45 60 75 90]);
set(gca,'ytick',[-2 -1 0 1 2]);
grid on

