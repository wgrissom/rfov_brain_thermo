doReverseTime = false;
baseInd = 1; % 2 for temp plots; 1 for using last baseline

useksh = false; % true for multibaseline study, false otherwise

imgNumsRe = (0:29)*4 + 3;
imgNumsIm = (0:29)*4 + 4;

% load full-EX data
for ii = 1:30
    imgFull(:,:,ii) = double(dicomread(['Axialfull30WithHeat_85/IM-0004-' sprintf('%04d',imgNumsRe(ii))]));
    imgFull(:,:,ii) = imgFull(:,:,ii) + ...
        1i * double(dicomread(['Axialfull30WithHeat_85/IM-0004-' sprintf('%04d',imgNumsIm(ii))]));
end

% load rFOV data
for ii = 1:30
    imgrFOV(:,:,ii) = double(dicomread(['AxialrFOV30WithHeat_83/IM-0002-' sprintf('%04d',imgNumsRe(ii))]));
    imgrFOV(:,:,ii) = imgrFOV(:,:,ii) + ...
        1i * double(dicomread(['AxialrFOV30WithHeat_83/IM-0002-' sprintf('%04d',imgNumsIm(ii))]));
end

if doReverseTime
    imgFull = flip(imgFull,3);
    imgrFOV = flip(imgrFOV,3);
end

teFull = 13; % ms
terFOV = 17.3; % ms
cFull = 1/(3*2*pi*42.58*0.01*teFull/1000);
crFOV = 1/(3*2*pi*42.58*0.01*terFOV/1000);

[x,y] = meshgrid(1:256);

% load phantom masks
if ~exist('axialPhantomMask.mat','file')
    addpath ~/code/uva_fusf_rthawk/inccaGradData
    mask = roiSelect(abs(imgFull(:,:,1))+abs(imgrFOV(:,:,1)));
    save axialPhantomMask.mat mask
else
    load axialPhantomMask.mat
end

maskXMean = mean(x(mask(:)));
maskYMean = mean(y(mask(:)));
mask = (x-round(maskXMean)).^2 + (y-round(maskYMean)-1).^2 <= 31^2; % radius 31

xInds = (-32:31) + round(maskXMean);
yInds = (-32:31) + round(maskYMean);

% hot spot mask should be centered at 131,95, radius 9 voxels
hotSpotMask = (x-131).^2 + (y-95).^2 <= 9^2;

% if ~exist('axialHotSpotMask.mat','file')
%     addpath ~/code/uva_fusf_rthawk/inccaGradData
%     hotSpotMask = roiSelect(max(temprFOV,[],3) > 1);
%     save axialHotSpotMask.mat hotSpotMask
% else
%     load axialHotSpotMask.mat
% end

if useksh
    N  = 256;
    
    for ii = 13;%baseInd+1:size(imgFull,3)
        
        printf('working on dynamic %d',ii);
        
        acqp = struct('k',[]); % image domain
        acqp.mask = true(N);
        acqp.fov = 1;
        algp = struct('dofigs',1);
        algp.order = 0; % polynomial order
        algp.modeltest = false;
        algp.beta = -1; % no spatial regularization
        acqp.thetaSign = 1;
        algp.lam = [10^-2 -1];
        
        thetainit = zeros(N,N); % initialize temp phase shift map with zeros
        
        % full FOV
        acqp.data = mask.*imgFull(:,:,ii); % heating image
        acqp.data = acqp.data(:);
        acqp.L = repmat(mask,[1 1 1]).*imgFull(:,:,baseInd);
        acqp.L = permute(acqp.L,[3 1 2]);
        acqp.L = acqp.L(:,:).'; % baseline
        [thetakacc,A,c] = kspace_hybrid_thermo(acqp,thetainit,algp);
        Ac = reshape(A*c,[N N]);
        tempFull(:,:,ii) = cFull*real(thetakacc);
        %tempFull(:,:,ii) = cFull*angle(imgFull(:,:,ii).*conj(exp(1i*Ac).*imgFull(:,:,baseInd)));
        
        % reduced FOV
        acqp.data = mask.*imgrFOV(:,:,ii); % heating image
        acqp.data = acqp.data(:);
        acqp.L = mask.*imgrFOV(:,:,baseInd);acqp.L = acqp.L(:); % baseline
        [thetakacc,A,c] = kspace_hybrid_thermo(acqp,thetainit,algp);
        Ac = reshape(A*c,[N N]);
        temprFOV(:,:,ii) = crFOV*real(thetakacc);
        %temprFOV(:,:,ii) = crFOV*angle(imgrFOV(:,:,ii).*conj(exp(1i*Ac).*imgrFOV(:,:,baseInd)));
        
    end
    tempFull = tempFull(:,:,baseInd+1:end);
    %tempFull = repmat(mask,[1 1 size(tempFull,3)]).*tempFull;
    temprFOV = temprFOV(:,:,baseInd+1:end);
    %temprFOV = repmat(mask,[1 1 size(temprFOV,3)]).*temprFOV;
    
else
    
    % calculate temperature std through-time and within phantom mask for each
    % time point and for whole time-series
    tempFull = cFull * angle(imgFull(:,:,baseInd + 1:end).*conj(repmat(imgFull(:,:,baseInd),[1 1 30-baseInd])));
    temprFOV = crFOV * angle(imgrFOV(:,:,baseInd + 1:end).*conj(repmat(imgrFOV(:,:,baseInd),[1 1 30-baseInd])));


    mask = mask & ~hotSpotMask;
    
    % mean-correct each time point
    for ii = 1:size(tempFull,3)
        tmp = tempFull(:,:,ii);
        meanVal = mean(tmp(mask));
        tempFull(:,:,ii) = tempFull(:,:,ii) - meanVal;
    end
    
    % mean-correct each time point
    for ii = 1:size(temprFOV,3)
        tmp = temprFOV(:,:,ii);
        meanVal = mean(tmp(mask));
        temprFOV(:,:,ii) = temprFOV(:,:,ii) - meanVal;
    end
    
end

tempStdFull = std(tempFull(repmat(mask,[1 1 30-baseInd])));
tempStdrFOV = std(temprFOV(repmat(mask,[1 1 30-baseInd])));

figure;
subplot(121)
imagesc(mask.*std(tempFull,[],3),[0 1]);axis image
title 'Full'
subplot(122)
imagesc(mask.*std(temprFOV,[],3),[0 1]);axis image
title 'rFOV'



% hot spot at 131 95
tr_rFOV = 43.892; 
yFrac = 0.65;
t_rFOV = (0:size(tempFull,3)-1)*128*yFrac*tr_rFOV/1000;
tr_full = 25.696;
t_full = (0:size(tempFull,3)-1)*128*tr_full/1000;
figure;
plot(t_full,squeeze(tempFull(95,131,:)));
hold on
plot(t_rFOV,squeeze(temprFOV(95,131,:)));
axis([0 90 -2 14])
axis square
set(gcf,'position',[801 584 469 241]);
set(gca,'xtick',[0 15 30 45 60 75 90]);
set(gca,'ytick',[-2 2 6 10 14]);

% make a plot of temperature errors in phantom versus time for each scan
for ii = 1:size(tempFull,3)
    tmp = tempFull(:,:,ii);
    allTempsFull(:,ii) = tmp(mask);
end
figure;hold on
%boxplot(allTempsFull);drawnow
plot(t_full,flip(std(allTempsFull,[],1)));
axis([0 90 0 4]);

for ii = 1:size(temprFOV,3)
    tmp = temprFOV(:,:,ii);
    allTempsrFOV(:,ii) = tmp(mask);
end
%figure
%boxplot(allTempsrFOV);drawnow
plot(t_rFOV,flip(std(allTempsrFOV,[],1)));
%axis([0 30 -2 2]);
axis square
legend('Full','rFOV');
set(gcf,'position',[801 584 469 241]);
set(gca,'xtick',[0 15 30 45 60 75 90]);
set(gca,'ytick',[0:0.5:4]);

% temp maps at peak heat
figure
subplot(221)
imagesc(tempFull(:,:,12),[-1 14]);axis image
subplot(222)
imagesc(temprFOV(:,:,11),[-1 14]);axis image
subplot(223)
imagesc(tempFull(:,:,12),[-1 1]);axis image
subplot(224)
imagesc(temprFOV(:,:,11),[-1 1]);axis image

% magnitude images
magImgs = abs([imgFull(:,:,baseInd) imgrFOV(:,:,baseInd)]);
magImgs = magImgs./4500;magImgs(magImgs > 4500) = 1;
magImgs = repmat(magImgs,[1 1 3]);
% temp maps - unwindowed
tempMaps = [tempFull(yInds,xInds,12) temprFOV(yInds,xInds,11)];
tempMaps(tempMaps > 14) = 14;
tempMaps(tempMaps < -2) = -2;
% temp Maps - windowed
tempMapsw = [tempFull(yInds,xInds,12) temprFOV(yInds,xInds,11)];
tempMapsw(tempMapsw > 3) = 3;
tempMapsw(tempMapsw < -3) = -3;

threeColor = zeros([size(tempMaps) 3]);
map = parula(64); % get a colormap
% interpolate temp errors onto colormap
for kk = 1:3 % loop over color channels
  threeColor(:,:,kk) = reshape(interp1(linspace(-2,14,64),map(:,kk),tempMaps(:)),size(tempMaps));
end

threeColorw = zeros([size(tempMaps) 3]);
map = parula(64); % get a colormap
% interpolate temp errors onto colormap
for kk = 1:3 % loop over color channels
  threeColorw(:,:,kk) = reshape(interp1(linspace(-3,3,64),map(:,kk),tempMapsw(:)),size(tempMapsw));
end

% display them
figure
subplot(311)
imagesc(magImgs);axis image
axis off
colorbar;

subplot(312)
imagesc(threeColor);axis image
axis off
h = colorbar; 
fontColor = [0.75 0.75 0.75];
set(h,'Limits',[-2 14],'Ticks',[-2 2 6 10 14],...
    'TickLabels',{'-2','2','6','10','14'},'FontSize',14,'Color',fontColor);
caxis([-2 14]);
% pos = get(h,'position');
% set(h,'position',[0.83 pos(2:3) 0.375]);
% h = text(585,392,'^{\circ}C','HorizontalAlignment','Left','FontSize',20,'Color',fontColor);


subplot(313)
imagesc(threeColorw);axis image
axis off
h = colorbar; 
fontColor = [0.75 0.75 0.75];
set(h,'Limits',[-3 3],'Ticks',-3:1.5:3,...
    'TickLabels',{'-3','-1.5','0','1.5','3'},'FontSize',14,'Color',fontColor);
caxis([-3 3]);
%pos = get(h,'position');
%set(h,'position',[0.83 pos(2:3) 0.375]);
%h = text(585,392,'^{\circ}C','HorizontalAlignment','Left','FontSize',20,'Color',fontColor);

% save images and parameters for making a movie
tr_Full = 25.7;
t_rFOV = (0:27)*tr_rFOV/1000*128*0.65;
t_Full = (0:27)*tr_full/1000*128;

save axial_results xInds yInds t_rFOV t_Full imgFull imgrFOV mask tempFull temprFOV;

% make a plot of temperature profiles at peak heat
figure
% horizontal (reduced) dimension
subplot(121)
x = 280/256*(-10:10); % x-locations
hold on
plot(x,squeeze(tempFull(95,131-10:131+10,11)));
plot(x,squeeze(temprFOV(95,131-10:131+10,10)));
axis square
axis([x(1) x(end) -2 14])
xlabel 'mm'
ylabel '\Delta ^{\circ}C'
grid on

% rFOV: -2.04 - 2.58 = 4.62
% full FOV: -2.13 - 2.4 = 4.53

% vertical (non-reduced) dimension
subplot(122)
hold on
plot(x,squeeze(tempFull(95-10:95+10,131,11)));
plot(x,squeeze(temprFOV(95-10:95+10,131,10)));
axis square
axis([x(1) x(end) -2 14])
xlabel 'mm'
ylabel '\Delta ^{\circ}C'
grid on

% rFOV: -1.61 - 2.91 = 4.52
% full-FOV: -1.32 - 2.34 = 3.66
