imgNumsRe = (0:29)*4 + 3;
imgNumsIm = (0:29)*4 + 4;

% load full-EX data
for ii = 1:30
    imgFull(:,:,ii) = double(dicomread(['Coronalfull30NoHeat_98/IM-0003-' sprintf('%04d',imgNumsRe(ii))]));
    imgFull(:,:,ii) = imgFull(:,:,ii) + ...
        1i * double(dicomread(['Coronalfull30NoHeat_98/IM-0003-' sprintf('%04d',imgNumsIm(ii))]));
end

% load rFOV data
for ii = 1:30
    imgrFOV(:,:,ii) = double(dicomread(['CoronalrFOV30NoHeat_96/IM-0001-' sprintf('%04d',imgNumsRe(ii))]));
    imgrFOV(:,:,ii) = imgrFOV(:,:,ii) + ...
        1i * double(dicomread(['CoronalrFOV30NoHeat_96/IM-0001-' sprintf('%04d',imgNumsIm(ii))]));
end

teFull = 13; % ms
terFOV = 17.3; % ms
cFull = -1/(3*2*pi*42.58*0.01*teFull/1000);
crFOV = -1/(3*2*pi*42.58*0.01*terFOV/1000);

% load phantom masks
if ~exist('coronalPhantomMask.mat','file')
    addpath ~/code/uva_fusf_rthawk/inccaGradData
    mask = roiSelect(abs(imgFull(:,:,1))+abs(imgrFOV(:,:,1)));
    save coronalPhantomMask.mat mask
else
    load coronalPhantomMask.mat
end

% calculate temperature std through-time and within phantom mask for each
% time point and for whole time-series
baseInd = 2;
tempFull = cFull * angle(imgFull(:,:,baseInd + 1:end).*conj(repmat(imgFull(:,:,baseInd),[1 1 30-baseInd])));
% mean-correct each time point
for ii = 1:size(tempFull,3)
    tmp = tempFull(:,:,ii);
    meanVal = mean(tmp(mask));
    tempFull(:,:,ii) = tempFull(:,:,ii) - meanVal;
end
tempStdFull = std(tempFull(repmat(mask,[1 1 30-baseInd])));

temprFOV = crFOV * angle(imgrFOV(:,:,baseInd + 1:end).*conj(repmat(imgrFOV(:,:,baseInd),[1 1 30-baseInd])));
% mean-correct each time point
for ii = 1:size(temprFOV,3)
    tmp = temprFOV(:,:,ii);
    meanVal = mean(tmp(mask));
    temprFOV(:,:,ii) = temprFOV(:,:,ii) - meanVal;
end
tempStdrFOV = std(temprFOV(repmat(mask,[1 1 30-baseInd])));

% make a box plot of temperatures in phantom versus time for each scan
for ii = 1:size(tempFull,3)
    tmp = tempFull(:,:,ii);
    allTempsFull(:,ii) = tmp(mask);
end
figure
boxplot(allTempsFull)
axis([0 30-baseInd+1 -3 3]);


for ii = 1:size(temprFOV,3)
    tmp = temprFOV(:,:,ii);
    allTempsrFOV(:,ii) = tmp(mask);
end
figure
boxplot(allTempsrFOV)
axis([0 30-baseInd+1 -3 3]);


