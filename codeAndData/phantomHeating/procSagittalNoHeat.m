
baseInd = 2;

imgNumsRe = (0:29)*4 + 3;
imgNumsIm = (0:29)*4 + 4;

% load full-EX data
for ii = 1:30
    imgFull(:,:,ii) = double(dicomread(['Sagittalfull30NoHeat_91/IM-0003-' sprintf('%04d',imgNumsRe(ii))]));
    imgFull(:,:,ii) = imgFull(:,:,ii) + ...
        1i * double(dicomread(['Sagittalfull30NoHeat_91/IM-0003-' sprintf('%04d',imgNumsIm(ii))]));
end

% load rFOV data
for ii = 1:30
    imgrFOV(:,:,ii) = double(dicomread(['SagittalrFOV30NoHeat_89/IM-0001-' sprintf('%04d',imgNumsRe(ii))]));
    imgrFOV(:,:,ii) = imgrFOV(:,:,ii) + ...
        1i * double(dicomread(['SagittalrFOV30NoHeat_89/IM-0001-' sprintf('%04d',imgNumsIm(ii))]));
end

teFull = 13; % ms
terFOV = 17.3; % ms
cFull = -1/(3*2*pi*42.58*0.01*teFull/1000);
crFOV = -1/(3*2*pi*42.58*0.01*terFOV/1000);

% load phantom masks
if ~exist('sagittalPhantomMask.mat','file')
    addpath ~/code/uva_fusf_rthawk/inccaGradData
    mask = roiSelect(abs(imgFull(:,:,1))+abs(imgrFOV(:,:,1)));
    save sagittalPhantomMask.mat mask
else
    load sagittalPhantomMask.mat
end

xInds = (-32:31) + 126;
yInds = (-32:31) + 136;

% calculate temperature std through-time and within phantom mask for each
% time point and for whole time-series

tempFull = cFull * angle(imgFull(:,:,baseInd + 1:end).*conj(repmat(imgFull(:,:,baseInd),[1 1 30-baseInd])));
temprFOV = crFOV * angle(imgrFOV(:,:,baseInd + 1:end).*conj(repmat(imgrFOV(:,:,baseInd),[1 1 30-baseInd])));

% mean-correct each time point
for ii = 1:size(tempFull,3)
    tmp = tempFull(:,:,ii);
    meanVal = mean(tmp(mask));
    tempFull(:,:,ii) = tempFull(:,:,ii) - meanVal;
end

% mean-correct each time point
for ii = 1:size(temprFOV,3)
    tmp = temprFOV(:,:,ii);
    meanVal = mean(tmp(mask));
    temprFOV(:,:,ii) = temprFOV(:,:,ii) - meanVal;
end

tempStdFull = std(tempFull(repmat(mask,[1 1 30-baseInd])))
tempStdrFOV = std(temprFOV(repmat(mask,[1 1 30-baseInd])))
tempMeanFull = mean(tempFull(repmat(mask,[1 1 30-baseInd])))
tempMeanrFOV = mean(temprFOV(repmat(mask,[1 1 30-baseInd])))

figure;
subplot(221)
imagesc(std(tempFull(yInds,xInds,:),[],3),[0 0.3]);axis image;axis off;colorbar
title 'Full'
subplot(222)
imagesc(std(temprFOV(yInds,xInds,:),[],3),[0 0.3]);axis image;axis off;colorbar
title 'rFOV'
subplot(223)
imagesc(mean(tempFull(yInds,xInds,:),3),[-1 1]);axis image;axis off;colorbar
title 'Full'
subplot(224)
imagesc(mean(temprFOV(yInds,xInds,:),3),[-1 1]);axis image;axis off;colorbar
title 'rFOV'

% hot spot at 129 127
tr_rFOV = 43.892; 
yFrac = 0.7;
t_rFOV = (0:size(tempFull,3)-1)*128*yFrac*tr_rFOV/1000;
tr_full = 25.696;
t_full = (0:size(tempFull,3)-1)*128*tr_full/1000;
figure;
plot(t_full,squeeze(tempFull(127,129,:)));
hold on
plot(t_rFOV,squeeze(temprFOV(127,129,:)));
axis([0 90 -1 1])
axis square
set(gcf,'position',[801 584 469 241]);
set(gca,'xtick',[0 15 30 45 60 75 90]);
set(gca,'ytick',[-1 -0.5 0 0.5 1]);
grid on



