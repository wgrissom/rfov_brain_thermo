doReverseTime = false;
baseInd = 1; % 2 for temp plots; 1 for using last baseline


imgNumsRe = (0:29)*4 + 3;
imgNumsIm = (0:29)*4 + 4;

% load full-EX data
for ii = 1:30
    imgFull(:,:,ii) = double(dicomread(['Sagittalfull30WithHeat_92/IM-0004-' sprintf('%04d',imgNumsRe(ii))]));
    imgFull(:,:,ii) = imgFull(:,:,ii) + ...
        1i * double(dicomread(['Sagittalfull30WithHeat_92/IM-0004-' sprintf('%04d',imgNumsIm(ii))]));
end

% load rFOV data
for ii = 1:30
    imgrFOV(:,:,ii) = double(dicomread(['SagittalrFOV30WithHeat_90/IM-0002-' sprintf('%04d',imgNumsRe(ii))]));
    imgrFOV(:,:,ii) = imgrFOV(:,:,ii) + ...
        1i * double(dicomread(['SagittalrFOV30WithHeat_90/IM-0002-' sprintf('%04d',imgNumsIm(ii))]));
end

if doReverseTime
    imgFull = flip(imgFull,3);
    imgrFOV = flip(imgrFOV,3);
end

teFull = 13; % ms
terFOV = 17.3; % ms
cFull = 1/(3*2*pi*42.58*0.01*teFull/1000);
crFOV = 1/(3*2*pi*42.58*0.01*terFOV/1000);

[x,y] = meshgrid(1:256);

% load phantom masks
if ~exist('sagittalPhantomMask.mat','file')
    addpath ~/code/uva_fusf_rthawk/inccaGradData
    mask = roiSelect(abs(imgFull(:,:,1))+abs(imgrFOV(:,:,1)));
    save sagittalPhantomMask.mat mask
else
    load sagittalPhantomMask.mat
end

% calculate temperature std through-time and within phantom mask for each
% time point and for whole time-series
tempFull = cFull * angle(imgFull(:,:,baseInd + 1:end).*conj(repmat(imgFull(:,:,baseInd),[1 1 30-baseInd])));
temprFOV = crFOV * angle(imgrFOV(:,:,baseInd + 1:end).*conj(repmat(imgrFOV(:,:,baseInd),[1 1 30-baseInd])));

if ~exist('sagittalHotSpotMask.mat','file')
    addpath ~/code/uva_fusf_rthawk/inccaGradData
    hotSpotMask = roiSelect(max(temprFOV,[],3) > 1);
    save sagittalHotSpotMask.mat hotSpotMask
else
    load sagittalHotSpotMask.mat
end
mask = mask & ~hotSpotMask;


% mean-correct each time point
for ii = 1:size(tempFull,3)
    tmp = tempFull(:,:,ii);
    meanVal = mean(tmp(mask));
    tempFull(:,:,ii) = tempFull(:,:,ii) - meanVal;
end

% mean-correct each time point
for ii = 1:size(temprFOV,3)
    tmp = temprFOV(:,:,ii);
    meanVal = mean(tmp(mask));
    temprFOV(:,:,ii) = temprFOV(:,:,ii) - meanVal;
end

tempStdFull = std(tempFull(repmat(mask,[1 1 30-baseInd])));
tempStdrFOV = std(temprFOV(repmat(mask,[1 1 30-baseInd])));

figure;
subplot(121)
imagesc(mask.*std(tempFull,[],3),[0 1]);axis image
title 'Full'
subplot(122)
imagesc(mask.*std(temprFOV,[],3),[0 1]);axis image
title 'rFOV'

% hot spot at 129 127
tr_rFOV = 43.892; 
yFrac = 0.7;
t_rFOV = (0:size(tempFull,3)-1)*128*yFrac*tr_rFOV/1000;
tr_full = 25.696;
t_full = (0:size(tempFull,3)-1)*128*tr_full/1000;
figure;
plot(t_full,squeeze(tempFull(127,129,:)));
hold on
plot(t_rFOV,squeeze(temprFOV(127,129,:)));
axis([0 90 -2 10])
axis square
legend('Full','rFOV');
set(gcf,'position',[801 584 469 241]);
set(gca,'xtick',[0 15 30 45 60 75 90]);
set(gca,'ytick',[-2 0 2 4 6 8 10]);

% make a plot of temperature errors in phantom versus time for each scan
for ii = 1:size(tempFull,3)
    tmp = tempFull(:,:,ii);
    allTempsFull(:,ii) = tmp(mask);
end
figure;hold on
%boxplot(allTempsFull);drawnow
plot(t_full,flip(std(allTempsFull,[],1)));
axis([0 90 0 4]);

for ii = 1:size(temprFOV,3)
    tmp = temprFOV(:,:,ii);
    allTempsrFOV(:,ii) = tmp(mask);
end
%figure
%boxplot(allTempsrFOV);drawnow
plot(t_rFOV,flip(std(allTempsrFOV,[],1)));
%axis([0 30 -2 2]);
axis square
legend('Full','rFOV');
set(gcf,'position',[801 584 469 241]);
set(gca,'xtick',[0 15 30 45 60 75 90]);
set(gca,'ytick',[0:0.5:4]);

% temp maps at peak heat
figure
subplot(221)
imagesc(tempFull(:,:,11),[-1 10]);axis image
subplot(222)
imagesc(temprFOV(:,:,9),[-1 10]);axis image
subplot(223)
imagesc(tempFull(:,:,11),[-1 1]);axis image
subplot(224)
imagesc(temprFOV(:,:,9),[-1 1]);axis image


% magnitude images
magImgs = abs([imgFull(:,:,baseInd) imgrFOV(:,:,baseInd)]);
magImgs = magImgs./4500;magImgs(magImgs > 4500) = 1;
magImgs = repmat(magImgs,[1 1 3]);

xInds = (-32:31) + 126;
yInds = (-32:31) + 136;

% temp maps - unwindowed
tempMaps = [tempFull(yInds,xInds,11) temprFOV(yInds,xInds,9)];
tempMaps(tempMaps > 10) = 10;
tempMaps(tempMaps < -2) = -2;
% temp Maps - windowed
tempMapsw = [tempFull(yInds,xInds,11) temprFOV(yInds,xInds,9)];
tempMapsw(tempMapsw > 2) = 2;
tempMapsw(tempMapsw < -2) = -2;

threeColor = zeros([size(tempMaps) 3]);
map = parula(64); % get a colormap
% interpolate temp errors onto colormap
for kk = 1:3 % loop over color channels
  threeColor(:,:,kk) = reshape(interp1(linspace(-2,10,64),map(:,kk),tempMaps(:)),size(tempMaps));
end

threeColorw = zeros([size(tempMaps) 3]);
map = parula(64); % get a colormap
% interpolate temp errors onto colormap
for kk = 1:3 % loop over color channels
  threeColorw(:,:,kk) = reshape(interp1(linspace(-2,2,64),map(:,kk),tempMapsw(:)),size(tempMapsw));
end

% display them
figure
subplot(311)
imagesc(magImgs);axis image
axis off
colorbar

subplot(312)
imagesc(threeColor);axis image;
axis off
h = colorbar; 
fontColor = [0.75 0.75 0.75];
set(h,'Limits',[-2 10],'Ticks',[-2 2 6 10],...
    'TickLabels',{'-2','2','6','10'},'FontSize',14,'Color',fontColor);
caxis([-2 10]);


subplot(313)
imagesc(threeColorw);axis image;
axis off
h = colorbar; 
fontColor = [0.75 0.75 0.75];
set(h,'Limits',[-2 2],'Ticks',-2:2,...
    'TickLabels',{'-2','-1','0','1','2'},'FontSize',14,'Color',fontColor);
caxis([-2 2]);

% save images and parameters for making a movie
tr_Full = 25.7;
tr_rFOV = 43.9;
t_rFOV = (0:27)*tr_rFOV/1000*128*0.7;
t_Full = (0:27)*tr_Full/1000*128;

save sagittal_results xInds yInds t_rFOV t_Full imgFull imgrFOV mask tempFull temprFOV;

% make a plot of temperature profiles at peak heat
figure
% horizontal (reduced) dimension
subplot(121)
x = 280/256*(-10:10); % x-locations
hold on
plot(x,squeeze(tempFull(127,129-10:129+10,11)));
plot(x,squeeze(temprFOV(127,129-10:129+10,9)));
axis square
axis([x(1) x(end) -2 10])
xlabel 'mm'
ylabel '\Delta ^{\circ}C'
grid on

% rFOV: -2.68 - 2.06 = 4.74
% full FOV: -3.17 - 1.91 = 5.08

% vertical (non-reduced) dimension
subplot(122)
hold on
plot(x,squeeze(tempFull(127-10:127+10,129,11)));
plot(x,squeeze(temprFOV(127-10:127+10,129,9)));
axis square
axis([x(1) x(end) -2 10])
xlabel 'mm'
ylabel '\Delta ^{\circ}C'
grid on

% rFOV: -3.09 - 5.07 = 8.16
% full-FOV: -3.52 - 5.03 = 8.55
