% load results
x = load('results_rFOV');
y = load('results_fullFOV');


fontColor = [0.75 0.75 0.75];

% show magnitude images
figure;
subplot(321)
imagesc(flip(flip(abs(y.imgSep(:,:,end)).',1),2),[0 1]);
axis image
colormap gray
axis off
colorbar

xi = -128:127;
yi = 0.6*xi;
subplot(322)
imagesc(yi,xi,flip(flip(abs(x.imgSep(:,:,end)).',1),2),[0 0.6]);
axis image
colormap gray
axis off
colorbar


% show a single time point temp map
subplot(323)
imagesc(flip(flip((y.tempErrors(:,:,1)).',1),2),[-3 3]);
axis image
set(gca,'colormap',parula);
axis off
h = colorbar;
set(h,'Limits',[-3 3],'Ticks',[-3 -1.5 0 1.5 3],'TickLabels',{'-3','-1.5','0','1.5','3'},'FontSize',12,'Color',fontColor);
caxis([-3 3]);
    

xi = -128:127;
yi = 0.6*xi;
subplot(324)
h = imagesc(yi,xi,flip(flip((x.tempErrors(:,:,1)).',1),2),[-3 3]);
axis image
set(gca,'colormap',parula);
axis off
h = colorbar;
set(h,'Limits',[-3 3],'Ticks',[-3 -1.5 0 1.5 3],'TickLabels',{'-3','-1.5','0','1.5','3'},'FontSize',12,'Color',fontColor);
caxis([-3 3]);

% show stdev maps and report values in brain Mask
subplot(325)
imagesc(flip(flip(std(y.tempErrors,[],3).',1),2),[0 3]);
axis image
set(gca,'colormap',parula);
axis off
colorbar

xi = -128:127;
yi = 0.6*xi;
subplot(326)
h = imagesc(yi,xi,flip(flip(std(x.tempErrors,[],3).',1),2),[0 3]);
axis image
set(gca,'colormap',parula);
axis off
colorbar

% show stdev maps and report values in brain Mask
figure
subplot(325)
imagesc(flip(flip(mean(y.tempErrors,3).',1),2),[-1 1]);
axis image
set(gca,'colormap',parula);
axis off
colorbar

xi = -128:127;
yi = 0.6*xi;
subplot(326)
h = imagesc(yi,xi,flip(flip(mean(x.tempErrors,3).',1),2),[-1 1]);
axis image
set(gca,'colormap',parula);
axis off
colorbar

load brainMask_fullFOV
tempStdFullFOV = std(y.tempErrors(repmat(brainMask,[1 1 28])))
tempMeanFullFOV = mean(y.tempErrors(repmat(brainMask,[1 1 28])))
load brainMask_rFOV
tempStdrFOV = std(x.tempErrors(repmat(brainMask,[1 1 28])))
tempMeanrFOV = mean(x.tempErrors(repmat(brainMask,[1 1 28])))

return


% make a movie
figure(6);clf
clear F
F(28) = struct('cdata',[],'colormap',[]);
for ii = 1:size(y.tempErrors,3)
    subplot(121)
    imagesc(flip(flip((y.tempErrors(:,:,ii)).',1),2),[-6 6]);
    axis image
    set(gca,'colormap',parula);
    axis off
    h = colorbar;
    set(h,'Limits',[-6 6],'Ticks',[-6 -3 0 3 6],'TickLabels',{'-6','-3','0','3','6'},'FontSize',12,'Color',fontColor);
    caxis([-6 6]);
    title('Full-FOV','FontSize',14,'Color',fontColor);
    
    xi = -128:127;
    yi = 0.6*xi;
    subplot(122)
    h = imagesc(yi,xi,flip(flip((x.tempErrors(:,:,ii)).',1),2),[-6 6]);
    axis image
    set(gca,'colormap',parula);
    axis off
    h = colorbar;
    set(h,'Limits',[-6 6],'Ticks',[-6 -3 0 3 6],'TickLabels',{'-6','-3','0','3','6'},'FontSize',12,'Color',fontColor);
    caxis([-6 6]);
    title('Reduced-FOV','FontSize',14,'Color',fontColor);
    h = text(150,0,'\Delta ^{\circ}C','FontSize',14,'Color',fontColor);
    
    set(gcf,'color',[0 0 0]);
    
    drawnow
    F(ii) = getframe(gcf);
    F(ii).cdata = F(ii).cdata(:,[72:244 390:567],:);
    pause
end

v = VideoWriter('undersampledTempErrors.mp4','MPEG-4');
v.FrameRate = 5;
v.Quality = 95;
open(v)
writeVideo(v,F)
close(v)

