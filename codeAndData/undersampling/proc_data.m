% add path to read_MR_rawdata
addpath ../../inccaGradData/read_MR
addpath ../../Nifti_Analyze
addpath ~/Desktop/k-space-thermometry
addpath ../../inccaGradData/

dataSelect = 'fullFOV'; % 'rFOV' or 'fullFOV'
switch dataSelect
    case 'rFOV'
        pFile = 'P61440.7';
        ct = -1/(3*2*pi*42.58*0.01*0.0173);
        load brainMask_rFOV
    case 'fullFOV'
        pFile = 'P56832.7';
        ct = -1/(3*2*pi*42.58*0.01*0.013);
        load brainMask_fullFOV
end

% water bath
N = 256; % number of frequency points
nDummies = 2; % = index of baseline image
%data = sqz(read_MR_rawdata([pwd '/../rFOVdata/' pFile])); % neg2slphs = OFF
load([pFile '.mat']);
Nt = size(data,3); % # of time points
Np = size(data,1); % number of phase encodes
data = data.*repmat(kron(ones(Np/2,1),[1 -1]'),[1 N Nt]); % undo signal chopping
data = conj(data);

imgSep = zeros(N,N,nDummies);
for ii = 1:nDummies
    dataSep = data(:,:,ii);
    datazp = zeros(N,N,size(dataSep,3));
    datazp((N-Np)/2+1:(N-Np)/2+Np,:,:) = dataSep;
    dataSep = datazp; clear datazp
    imgSep(:,:,ii) = ift2(dataSep);
end

if ~exist('brainMask_rFOV.mat','file')
    brainMask = roiSelect(abs(imgSep(:,:,1,end)));
    save brainMask_rFOV.mat brainMask
else
    load brainMask_rFOV.mat
end
brainMask = brainMask & abs(imgSep(:,:,:,end)) > 0.125;

% zero pad the data to square
datazp = zeros(N,N,size(data,3));
datazp((N-Np)/2+1:(N-Np)/2+Np,:,:) = data;
data = datazp; clear datazp

% zet up precision calculation
L = permute(imgSep(:,:,end)*N*N,[1 2 4 3]); 

tempErrors = zeros(N,N,Nt);
parfor ii = nDummies+1:Nt
    
    printf('working on dynamic %d',ii);
    
    inds = 1:2:N; % 2x acceleration by skipping every other line
    dacc = col(data(inds,:,ii));
    acqp = struct('data',dacc);
    acqp.k = false(N,1);
    acqp.k(inds,:) = true;
    acqp.L = L(:);
    
    acqp.brainMask = true(N,N);
    
    algp = struct('dofigs',1);
    algp.order = 0; % polynomial order
    algp.modeltest = true;
    algp.beta = -1; % no spatial regularization
    
    thetainit = zeros(N,N); % initialize temp phase shift map with zeros
    
    thetakacc = kspace_hybrid_thermo(acqp,thetainit,algp);
    tempErrors(:,:,ii) = ct*real(thetakacc);

end
tempErrors = tempErrors(:,:,nDummies+1:end);

save(['results_' dataSelect],'tempErrors','imgSep');


