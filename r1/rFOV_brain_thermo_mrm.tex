\documentclass[11pt]{article}
%\usepackage{algorithm2e}
%\usepackage{algorithmicx}
%\usepackage{algorithm}
%\usepackage[noend]{algpseudocode}

\usepackage{dsfont}
\usepackage{relsize}
\usepackage{color}

%changing the Eq. tag to use [] when numbering. use \eqref{label} to reference equations in text.
\makeatletter
  \def\tagform@#1{\maketag@@@{[#1]\@@italiccorr}}
\makeatother

\linespread{1.5}
%\setlength{\parindent}{0in}

% the following command can be used to mark changes made due to reviewers' concerns. Use as \revbox{Rx.y} for reviewer x, concern y.
\newif\ifmarkedup
\markeduptrue

\ifmarkedup
	\newcommand{\revbox}[1]{\marginpar{\framebox{\textcolor{blue}{#1}}}}
\else
	\newcommand{\revbox}[1]{}
	\renewcommand{\textcolor}[1]{}
\fi

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}

\usepackage{textcomp}
\usepackage{url}
\usepackage{geometry}
\usepackage[cmex10]{amsmath}
\usepackage{amssymb}
\usepackage{topcapt}
\usepackage{booktabs}
\usepackage{times}
\usepackage{bm}
\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{array}
\usepackage{color}
%\usepackage[nomarkers]{endfloat/endfloat}
\usepackage[outerbars]{changebar}

\usepackage[sort&compress]{natbib}
\bibpunct{[}{]}{,}{n}{,}{,}

\DeclareMathOperator*{\argmin}{arg\!\min}

%changing the Eq. tag to use [] when numbering. use \eqref{label} to reference equations in text.
\makeatletter
  \def\tagform@#1{\maketag@@@{(#1)\@@italiccorr}}
\makeatother

\usepackage{ifpdf}
\ifpdf
\usepackage[pdftex]{graphicx}
\usepackage{epstopdf}
\else
	\usepackage[dvips]{graphicx}
	\usepackage{color,psfrag}
\fi


\begin{document}

% paper title
\title{Reducing temperature errors in transcranial MR-guided focused ultrasound using a reduced-field-of-view sequence}
\author{William A Grissom$^{1,2}$ and Steven Allen$^{3}$}
\maketitle
\begin{flushleft}
$^1$Vanderbilt University Institute of Imaging Science \\
$^2$Department of Biomedical Engineering, Vanderbilt University, Nashville, TN, United States \\
$^3$Department of Biomedical Engineering, University of Virginia, Charlottesville, VA, United States \\
Word Count: Approximately 2500\\
Running Title: Reduced-FOV Brain Thermometry \\
Corresponding author: \\
William Grissom \\
Vanderbilt University Institute of Imaging Science\\
1161 21st Avenue South\\
Medical Center North, AA-3114\\
Nashville, TN 37235 USA \\
E-mail: will.grissom@vanderbilt.edu \\
\end{flushleft}
\thispagestyle{plain}

\pagebreak

\section*{Abstract}
{\bf Purpose:}
To reduce temperature errors due to water motion in transcranial MR-guided focused ultrasound (tcMRgFUS) ablation.
\\
{\bf Theory and Methods:}
In tcMRgFUS, water is circulated in the transducer bowl around the patient's head for acoustic coupling and heat removal.
The water moves during sonications that are monitored by MR thermometry,
which causes it to alias into the brain and create temperature errors.
To reduce these errors, a two-dimensional excitation pulse was implemented in a
gradient-recalled echo thermometry sequence.
The pulse suppresses water signal by selectively exciting the brain only, which reduces the imaging FOV.
Improvements in temperature precision compared to the conventional scan were evaluated in healthy subject scans
outside the tcMRgFUS system,
gel phantom scans in the system with heating,
and in 2$\times$-accelerated head phantom scans in the system without heating.
\\
{\bf Results:}
In vivo temperature precision (standard deviation of temperature errors) was improved 29\% on average,
due to the longer TR of the reduced-FOV sequence.
In the phantom heating experiments, the hot spot was less distorted in the reduced-FOV scans, and background
temperature precision was improved 70\% on average.
In the accelerated head phantom temperature reconstructions,
reconstructions from the reduced-FOV sequence were improved 89\%.
\\
{\bf Conclusion:}
Reduced-FOV temperature imaging alleviates temperature errors due to water bath motion in tcMRgFUS,
and enables accelerated temperature mapping with greater precision.
%It is most effective when used in axial and coronal slice orientations, where the water bath occupies large portions of the FOV
%in the phase-encoded dimension.
\\[1em]
{\bf Key words:} temperature imaging; radiofrequency pulses; selective excitation; multidimensional excitation; high-intensity focused ultrasound; MRI-guided focused ultrasound

\pagebreak

\section*{Introduction}
%Introducing MRgFUS at a high level
Transcranial magnetic resonance imaging-guided focused ultrasound (tcMRgFUS) is a non-invasive ablative surgical technique which is FDA-approved for the treatment of essential tremor \cite{Elias:2013fh},
and is under development for other applications including neuropathic pain,
obsessive compulsive disorder, and brain tumors \cite{ghanouni:2015}.
During tcMRgFUS treatment, ultrasound energy is delivered from a hemispherical transducer through the skull to a specific location within the brain, without affecting tissue outside the target.
The primary role of MRI during MRgFUS treatments is to provide images with soft tissue contrast for targeting,
and to provide real-time temperature measurements in the target tissue
using the proton resonance frequency (PRF) shift method.

%Need for vol. thermometry (specifically for brain apps)
tcMRgFUS temperature imaging uses a real-time single-slice gradient echo 2DFT scan oriented in the axial, coronal, or sagittal planes,
and temperature maps are calculated from the phase shift between a baseline (pre-heating) image and each image acquired during heating.
However, the precision of the temperature maps is degraded in multiple ways
by the presence of the water bath that couples acoustic energy from the transducer to the head,
and is circulated between sonications to cool the skull.
First, the water bath prevents the use of local imaging coils,
so the body coil is used for transmit and receive,
which provides relatively low image SNR and consequently low temperature precision \cite{Rieke:2008ab}.
Second, the water bath requires a large imaging FOV (usually 28 cm) to prevent it aliasing into the brain.
Even with the large imaging FOV,
the water bath's motion within an image acquisition (3.3 seconds/image) causes it to alias in the phase encode dimension, which creates
phase errors that vary in both space and time and reduce temperature precision.
This effect is most pronounced at the beginning of a sonication,
when water circulation has just been switched off but the water continues to move due to its remaining inertia.
Water motion continues throughout the 15-30 second sonication,
and the ultrasound can cause additional water motion during sonications.
While there is ongoing research into doping and other methods to eliminate the water bath signal,
these will not be clinically available in the near future,
since a solution must be found that is non-toxic,
does not modify the speed of sound,
can be circulated and maintains water's capacity for removing heat from the skull,
does not lower the acoustic cavitation threshold,
and can be obtained in the large volumes required to fill the transducer and cooling and degassing circuits.

%Our method and how it overcomes the limitations of current methods
This work describes the use of an RF pulse to reduce temperature errors in tcMRgFUS by
suppressing water bath signal on either side of the head,
which reduces the imaging FOV in the phase-encoded dimension.
There are two common RF pulse-based approaches to eliminate unwanted signals that would otherwise alias into a volume of interest.
The first is outer volume suppression, wherein one or more spatially-selective RF pulses are applied to produce 90 degree excitations of unwanted tissue regions followed by crusher gradients
to eliminate the tissue's longitudinal magnetization immediately prior to the imaging excitation and readout \cite{le1998optimized,Tran:2000:Magn-Reson-Med:10642728,Luo:2001:Magn-Reson-Med:11378888,Pisani:2007:Magn-Reson-Med:17260360,henning2008selovs}.
In tcMRgFUS, outer volume suppression is limited by the very low transmit efficiency and large $B_1^+$ inhomogeneities inside the transducer and water bath.
The second approach which is immune to $B_1^+$ errors is a two-dimensional excitation that selectively excites only the region of interest, within the slice of interest \cite{Rieseberg:2002aa,saritas2008dsc,Alley:1997aa}.
%and limits the required encoding FOV in the phase encode dimension.
In this work, a two-dimensional excitation was implemented in the pulse sequence that is
used clinically for tcMRgFUS temperature monitoring.
%to achieve a reduced-FOV that eliminates most of the water bath signal in the imaging phase encoded dimension.
The resulting sequence was compared to the conventional full-FOV scan in healthy subjects outside the transducer,
in gel phantoms in the transducer with FUS heating,
and in a head phantom in the transducer without FUS heating but with k-space undersampling.

\section*{Methods}

\subsection*{\textnormal{Pulse Design and Imaging Sequences}}
A two-dimensional RF pulse was implemented in the RF-spoiled gradient-recalled echo 2DFT sequence (\texttt{fgre}) used
clinically for brain thermometry, on a GE Discovery MR750T 3T scanner (GE Healthcare, Waukesha, WI, USA)
equipped with an Insightec ExAblate Neuro 650 focused ultrasound system (Insightec Ltd, Tirat Carmel, IL).
Figure \ref{fig:pulses} plots the pulse's RF and gradient waveforms.
The pulse was designed by duplicating the original sequence's time-bandwidth product 2 slice-select RF pulse waveform
10 times, and multiplying each copy by its corresponding position in a minimum-phase space-bandwidth product 4 small-tip-angle RF envelope,
which was designed using the Shinnar-Le Roux algorithm \cite{Pauly:1991lr}.
The subpulses were aligned with the positive plateaus of a repeating z-gradient trapezoid without ramp sampling,
which traced out a flyback trajectory.
$G_y$ phase encode blips were centered with each z-gradient rewinder.
A bipolar version of the pulse was also implemented but the flyback trajectory was used for all experiments due to its robustness to
gradient and timing errors, as discussed below.
The default slab width was 100 mm, with an excitation FOV of 250 mm.
The slab width and excitation FOV were adjusted as needed for each scan by scaling the phase encode blips.
The position of the slab was also controlled by adjusting a linear phase ramp across the subpulses.
The gradient trapezoids were designed for minimum duration,
subject to peak gradient amplitude and slew rate limits of 40 mT/m and 120 mT/m/ms,
and each had an amplitude of 30 mT/m and a duration of 1.032 ms,
for a total pulse duration of 20.24 ms.
All scans used a 3 mm slice thickness and a 30 degree flip angle.
The TE/TR of the full-FOV sequence were 13/25.6 ms, and the TE/TR of the reduced-FOV sequence were 17.3/43.9 ms.
The full image FOV in each case was 280 $\times$ 280 mm, with a 256 $\times$ 256 reconstructed matrix size.
The full-FOV scan measured 128 phase encode lines,
and had a scan time of 3.29 seconds/image.
The readout bandwidth was 5.68 kHz.
Thirty time points were measured in every scan.

\subsection*{\textnormal{In Vivo Temperature Precision}}
The conventional and reduced-FOV scans are expected to have different temperature precision
due to the reduced-FOV sequence's longer TR (which increases image SNR) and its reduced phase encoding (which lowers image SNR).
To compare the scans, images were acquired in axial, coronal, and sagittal planes during free breathing in a healthy volunteer
outside the water bath,
with approval of the Institutional Review Board at the University of Virginia.
The subject's head was supported inside a head birdcage coil,
but the body coil was used for imaging since it is used for all scans with the transducer.
The reduced-FOV scans used slab widths of 120 mm (axial and coronal) and 200 mm (sagittal),
and encoded y-FOV's of 168 mm/60\% (axial and coronal) and 196 mm/70\% (sagittal).
The resulting scan times were 3.37 seconds/image (axial and coronal) and 3.93 seconds/image (sagittal).
Temperature maps were calculated by the hybrid multibaseline and referenceless method \cite{grissom2010hybrid}
using the second image in the time series as the baseline,
and a second order referenceless phase polynomial \cite{Rieke:2013ek}.

\subsection*{\textnormal{Gel Phantom FUS Heating}}
To compare the full- and reduced-FOV in a scan with FUS heating,
a hemispherical gel phantom was placed in the Insightec system and sonicated while imaging
in axial and sagittal planes.
A coronal experiment was omitted since, due to circular symmetry of both phantom and transducer, the geometry was the same as the sagittal experiment.
Sonications were started 15 seconds after the scans started,
and lasted 30 seconds at 15 Watts acoustic power.
The Insightec system managed water in the same manner as in a human surgery: the system automatically circulated, cooled, and degassed water between sonications and switched off circulation at the start of each scan.
Due to the phantom's small size, the reduced-FOV scans used slab thicknesses of 70 mm (axial and sagittal),
and encoded y-FOV's of 182 mm/65\% (axial) and 210 mm/70\% (sagittal).
The resulting scan times were 3.65 seconds/image (axial) and 4.21 seconds/image (sagittal).
Temperature maps were calculated by the hybrid multibaseline and referenceless method \cite{grissom2010hybrid}
using the second image in the time series as the baseline, and a zeroth order (constant)
referenceless phase polynomial.

\subsection*{\textnormal{Accelerated Head Phantom Precision}}
Several methods to accelerate brain temperature scans by k-space undersampling have been proposed
to increase the volume or number of slices that can be imaged
without decreasing frame rate \cite{todd2009reconstruction,Todd:2010jx,fielden:ismrm14,gaur:2015}.
However, these methods all rely on temporal redundancies in the data, which are obscured by unpredictable water motion \cite{gaur:jtu:2017}.
Hence, eliminating water bath signal in the undersampled dimension may improve undersampled temperature precision.
To evaluate improvements in temperature precision with the reduced-FOV scan and 2$\times$ k-space undersampling,
full- and reduced-FOV axial scans were acquired in an anthropomorphic head phantom \cite{guerin:mrm:2015} in the transducer
with the water bath filled but without heating.
The reduced-FOV scan used a slab width of 110 mm and an encoded y-FOV of 168 mm/60\%,
for a scan time of 3.37 seconds/image.
The second time point images were reconstructed as baselines and subsequent data were retrospectively undersampled by a factor
of 2, to achieve effective scan times of 1.65 seconds/image (full-FOV) and 1.69 seconds/image (reduced-FOV).
Temperature maps were reconstructed using the k-space hybrid algorithm \cite{gaur:2015}, and a zeroth order (constant)
referenceless phase polynomial.
As in the phantom heating experiments,
the Insightec system automatically managed water circulation and switched
it off at the start of each scan.

\par Data and source code to generate Figures 2--5 in this paper is available at \\ https://bitbucket.org/wgrissom/rfov\_brain\_thermo/.

\section*{Results} % where to include permutation figure?

\subsection*{\textnormal{In Vivo Temperature Precision}}
Figure \ref{fig:invivo} shows magnitude images and through-time temperature standard deviation maps for
the three slice orientations and the full-FOV and reduced-FOV sequences.
The reduced-FOV magnitude images had a higher peak signal (approximately 30\% higher)
than the full-FOV images due to their longer TR.
In the coronal and sagittal orientations,
off-resonance caused tissue in a few regions at the edge of the brain
to fall out of the two-dimensional pulse's passband,
which reduced flip angles (indicated by arrows).
Through-time temperature standard deviation maps are shown in the bottom row of the figure
inside a tissue mask which was the same for both scans.
The reduced-FOV sequence generally had lower temperature errors,
especially in the midbrain which is the target of essential tremor treatment.
The temperature standard deviations taken over both space and time within voxels with non-zero estimated temperatures
were 0.36 $^{\circ}$C (full-FOV) and 0.23 $^{\circ}$C (reduced-FOV) for the axial case,
0.32 $^{\circ}$C (full-FOV) and 0.24 $^{\circ}$C (reduced-FOV) for the coronal case,
and 0.18 $^{\circ}$C (full-FOV) and 0.14 $^{\circ}$C (reduced-FOV) for the sagittal case.
The hybrid reconstruction suppressed temperatures in the regions of signal loss in the coronal and sagittal reduced-FOV images,
but these were usually regions where the full-FOV image also had high temperature standard deviations.

\subsection*{\textnormal{Gel Phantom FUS Heating}}
Figure \ref{fig:phantomMaps} shows axial and sagittal phantom baseline images and temperature maps at peak heat.
The reduced-FOV pulse successfully excited the phantom region in both cases,
but due to the relatively narrow slab width (70 mm) the pulse also excited the outer edges of water bath,
which were prevented from aliasing into the phantom by appropriate adjustment of the phase encode FOV (orange arrows).
The full-FOV temperature maps contained large spatially-varying errors of more than 3 $^{\circ}$ C due to water aliasing,
which were absent from the reduced-FOV maps.
The aliasing also distorted the hot spots, which were more symmetric in the reduced-FOV maps.
In separate scans without heating, the background temperature standard deviations over space and time were measured to be
1.03 $^{\circ}$C (full-FOV) and 0.34 $^{\circ}$C (reduced-FOV) for the axial case,
and 0.85 $^{\circ}$C (full-FOV) and 0.23 $^{\circ}$C (reduced-FOV) for the sagittal case.
Figure \ref{fig:phantomPlots} plots temperature versus time in the voxel with highest heating for each scan and slice orientation;
in each slice orientation the voxel was in the same location in the full- and reduced-FOV maps.
The peak temperatures largely agree though there is a positive bias in the full-FOV curves before and after heating.
The reduced-FOV curves are also qualitatively smoother.

\subsection*{\textnormal{Accelerated Head Phantom Precision}}
The first row of Figure \ref{fig:undersampling} shows full- and reduced-FOV baseline image magnitudes.
In this scenario with a more realistic head size and slab width, the water bath edges were not excited.
The second row shows temperature error maps for the first time point in the series,
immediately after the baseline image,
and the third row shows through-time temperature standard deviation maps.
The full-FOV maps show that even immediately after the baseline image
there are large signal changes in the water bath that alias into the brain.
In contrast, the reduced-FOV temperature maps had small errors throughout the time series.
In the anterior and posterior brain regions where the water bath did not alias into the brain,
the full-FOV temperature errors and standard deviations were similar to the reduced-FOV values.
Temperature standard deviations over space and time
in the phantom brain region were 6.21 $^{\circ}$C (full-FOV) and 0.69 $^{\circ}$C (reduced-FOV).

\section*{Discussion}
The experimental results in this work showed that a two-dimensional RF pulse can be used to suppress water
bath signals on either side of the head during tcMRgFUS,
which improves temperature precision by preventing moving water from aliasing into the brain
and interfering with heating-induced phase shifts.
The in vivo results showed that
temperature precision was improved inside the brain  (29\% on average) in the reduced-FOV scans,
including regions targeted by tcMRgFUS treatment,
which was likely due to the longer TR of the reduced-FOV sequence.
There were some regions around the periphery of the brain where
signal was lost because the tissue's off resonance caused it to shift out of the pulse's passband.
However, these regions also coincided with regions of poor temperature precision in the original scan.
The TE starting time was determined by the isodelay of the minimum phase envelope used in the pulse;
finer control over the TE starting time could be achieved using a more flexible pulse design as in Ref. \cite{grissom2009maximum}.
In the gel phantom heating experiments, the hot spots appeared more symmetric
than in the conventional scans, and temperature curves in the voxel with peak heating
appeared smoother.
In the absence of heating in the gel phantom, the background temperature precision was improved 70\% in the gel phantom.
Finally, the anthropomorphic head phantom temperature reconstructions showed that the reduced-FOV scan
is more amenable to acceleration by k-space undersampling than the conventional scan,
and that 2$\times$-undersampled temperature precision was improved 89\%.
In addition to the k-space hybrid method we used for these undersampled reconstructions,
suppressing the time varying water bath signal should also improve the
quality of other temperature reconstructions that rely on temporal redundancies in the data,
including temporally constrained reconstruction \cite{todd2009reconstruction},
model-predictive filtering \cite{Todd:2010jx}, and Kalman filtering \cite{DenisdeSenneville:2013fh,fielden:ismrm14}.
In this work, the two-dimensional RF pulse was implemented in the single-echo thermometry scan that is most
widely used for tcMRgFUS,
but it should also be compatible with the alternative multiecho scan that has more recently become available,
and suffers less blurring in the frequency encode dimension due to chemical shift.

\par Presently the main limitation of the reduced-FOV sequence is the long duration of the RF pulse,
which increased the sequence’s minimum TR and TE compared to the conventional excitation,
and made it sensitive to off-resonance.
This long duration was principally a consequence of the flyback design,
which was chosen to avoid poor stopband suppression that we observed using a bipolar design,
which we believe was due to eddy currents with a spherical-type spatial dependence that were
induced by the slice-select gradients in the conductive transducer bowl.
Our bipolar design had a duration of 11 ms (9 ms shorted than flyback).
The flyback and bipolar stopband suppressions were identical outside the transducer,
so it is possible that bipolar pulses could be used in future transducer designs that support less eddy currents.
The pulse duration could also be reduced using ramp sampling without increasing slew rate or max gradient strength \cite{Grissom:2012:NUSLR},
but this may result in slice profile distortion requiring gradient calibration,
and was beyond the scope of this work.
The pulse duration could also be shortened on more recent scanners with higher slew rates.
Finally, it may be possible to shorten the pulse by removing a few subpulses (i.e. reducing the distance between passband replicas)
so long as the edges of the slab replicas sit outside the water bath,
which has a width of approximately 320 mm at its widest point.
Combined with the reduced phase encoding,
a shorter pulse could make the scan short enough that imaging additional slices would
not significantly increase the current 3.3 second/image frame rate.

%\par While last image could be retrospectively used as a baseline to correct temp maps post-ablation,
%this would not be compatible with real-time ablation control which is used on the Insightec system


\section*{Conclusions}
Reduced-FOV temperature imaging using a two-dimensional excitation pulse
alleviates temperature errors due to water bath motion,
and enables accelerated temperature mapping with greater precision.
In practice it will be most effective when used in axial and coronal slice orientations,
where the water bath occupies a large portion of the FOV
in the phase-encoded dimension.

% use section* for acknowledgement
\section*{Acknowledgments}
This work was supported by NIH grant R01 EB016695 and the Focused Ultrasound Foundation.
The authors would like to thank Charlotte Sappo for providing the anthropomorphic head phantom.

% references section
\bibliographystyle{cse}
%\bibliography{kspacehybrid}
\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }

\bibitem{Elias:2013fh}
Elias WJ, Huss D, Voss T, Loomba J, Khaled M, Zadicario E, Frysinger RC,
  Sperling SA, Wylie S, Monteith SJ, Druzgal J, Shah BB, Harrison M, Wintermark
  M.
\newblock A pilot study of focused ultrasound thalamotomy for essential tremor.
\newblock New Engl J Med 2013;\hspace{0pt}369:640--648.

\bibitem{ghanouni:2015}
Ghanouni P, Butts~Pauly K, Elias WJ, Henderson J, Sheehan J, Monteith S,
  Wintermark M.
\newblock Transcranial {MRI}-guided focused ultrasound: {A} review of the
  technologic and neurologic applications.
\newblock Am J Roentegnol 2015;\hspace{0pt}205:150--159.

\bibitem{Rieke:2008ab}
Rieke V, Pauly KB.
\newblock {MR} thermometry.
\newblock J Magn Reson Imag 2008;\hspace{0pt}27:376--390.

\bibitem{le1998optimized}
Le~Roux P, Gilles RJ, McKinnon GC, Carlier PG.
\newblock Optimized outer volume suppression for single-shot fast spin-echo
  cardiac imaging.
\newblock J Magn Reson Imag 1998;\hspace{0pt}8:1022--1032.

\bibitem{Tran:2000:Magn-Reson-Med:10642728}
Tran TK, Vigneron DB, Sailasuta N, Tropp J, Le~Roux P, Kurhanewicz J, Nelson S,
  Hurd R.
\newblock Very selective suppression pulses for clinical mrsi studies of brain
  and prostate cancer.
\newblock Magn Reson Med 2000;\hspace{0pt}43:23--33.

\bibitem{Luo:2001:Magn-Reson-Med:11378888}
Luo Y, de~Graaf RA, DelaBarre L, Tann{\'u}s A, Garwood M.
\newblock {BISTRO}: {An} outer-volume suppression method that tolerates {RF}
  field inhomogeneity.
\newblock Magn Reson Med 2001;\hspace{0pt}45:1095--1102.

\bibitem{Pisani:2007:Magn-Reson-Med:17260360}
Pisani L, Bammer R, Glover G.
\newblock Restricted field of view magnetic resonance imaging of a dynamic time
  series.
\newblock Magn Reson Med 2007;\hspace{0pt}57:297--307.

\bibitem{henning2008selovs}
Henning A, Sch\"ar M, Schulte RF, Wilm B, Pruessmann KP, Boesiger P.
\newblock {SELOVS}: {Brain} {MRSI} localization based on highly selective
  {T1}-and {B1}-insensitive outer-volume suppression at {3T}.
\newblock Magn Reson Med 2008;\hspace{0pt}59:40--51.

\bibitem{Rieseberg:2002aa}
Rieseberg S, Frahm J, Finsterbusch J.
\newblock Two-dimensional spatially-selective {RF} excitation pulses in
  echo-planar imaging.
\newblock Magn Reson Med 2002;\hspace{0pt}47:1186--1193.

\bibitem{saritas2008dsc}
Saritas E, Cunningham C, Lee J, Han E, Nishimura D.
\newblock {DWI} of the spinal cord with reduced {FOV} single-shot {EPI}.
\newblock Magn Reson Med 2008;\hspace{0pt}60:468--73.

\bibitem{Alley:1997aa}
Alley MT, Pauly JM, Sommer FG, Pelc NJ.
\newblock Angiographic imaging with {2D RF} pulses.
\newblock Magn Reson Med 1997;\hspace{0pt}37:260--267.

\bibitem{Pauly:1991lr}
Pauly JM, Le~Roux P, Nishimura DG, Macovski A.
\newblock Parameter relations for the {Shinnar-Le Roux} selective excitation
  pulse design algorithm.
\newblock IEEE Trans Med Imag 1991;\hspace{0pt}10:53--65.

\bibitem{grissom2010hybrid}
Grissom WA, Rieke V, Holbrook AB, Medan Y, Lustig M, Santos J, McConnell MV,
  Pauly KB.
\newblock Hybrid referenceless and multibaseline subtraction {MR} thermometry
  for monitoring thermal therapies in moving organs thermometry for monitoring
  thermal therapies in moving organs.
\newblock Med Phys 2010;\hspace{0pt}37:5014--5026.

\bibitem{Rieke:2013ek}
Rieke V, Instrella R, Rosenberg J, Grissom WA, Werner B, Martin E, Butts~Pauly
  K.
\newblock {Comparison of temperature processing methods for monitoring focused
  ultrasound ablation in the brain}.
\newblock J Magn Reson Imag 2013;\hspace{0pt}38:1462--1471.

\bibitem{todd2009reconstruction}
Todd N, Adluru G, Payne A, DiBella EVR, Parker DL.
\newblock Temporally constrained reconstruction applied to {MRI} temperature
  data.
\newblock Magn Reson Med 2009;\hspace{0pt}62:406--419.

\bibitem{Todd:2010jx}
Todd N, Payne A, Parker DL.
\newblock {Model predictive filtering for improved temporal resolution in MRI
  temperature imaging}.
\newblock Magn Reson Med 2010;\hspace{0pt}63:1269--1279.

\bibitem{fielden:ismrm14}
Fielden SW, Zhao L, Miller W, Feng X, Wintermark M, Butts~Pauly K, Meyer CH.
\newblock Accelerating {3D} spiral {MR} thermometry with the {Kalman} filter.
\newblock In Proc Intl Soc Mag Reson Med. 2014;\hspace{0pt} p. 2346.

\bibitem{gaur:2015}
Gaur P, Grissom WA.
\newblock Accelerated {MRI} thermometry by direct estimation of temperature
  from undersampled k-space data.
\newblock Magn Reson Med 2015;\hspace{0pt}73:1914--1925.

\bibitem{gaur:jtu:2017}
Gaur P, Werner B, Feng X, Fielden SW, Meyer CH, Grissom WA.
\newblock Spatially-segmented undersampled {MRI} temperature reconstruction for
  {MR}-guided focused ultrasound.
\newblock J Therapeutic Ultrasound 2017;\hspace{0pt}5:13.

\bibitem{guerin:mrm:2015}
Guerin B, Stockmann JP, Baboli M, Torrado-Carvajal A, Stenger VA, Wald LL.
\newblock Robust time-shifted spoke pulse design in the presence of large
  {$B_0$} variations with simultaneous reduction of through-plane dephasing,
  {$B_1^+$} effects, and the specific absorption rate using parallel
  transmission.
\newblock Magn Reson Med 2016;\hspace{0pt}76:540--554.

\bibitem{grissom2009maximum}
Grissom WA, Kerr AB, Holbrook AB, Pauly JM, Butts-Pauly K.
\newblock Maximum linear-phase spectral-spatial radiofrequency pulses for
  fat-suppressed proton resonance frequency-shift {MR} thermometry.
\newblock Magn Reson Med 2009;\hspace{0pt}62:1242--1250.

\bibitem{DenisdeSenneville:2013fh}
Denis~de Senneville B, Roujol S, Hey S, Moonen C, Ries M.
\newblock {Extended Kalman Filtering for Continuous Volumetric MR-Temperature
  Imaging}.
\newblock {IEEE} Trans Med Imag 2013;\hspace{0pt}32:711--718.

\end{thebibliography}



\pagebreak

\section*{Figure captions}

\begin{figure}[!h]
\centering
%\includegraphics[width=\textwidth]{figures/Fig1pulses.eps}
\caption{Two-dimensional RF pulse and gradient waveforms.
The z (slice) gradient is plotted in black, and the y (phase) gradient is plotted in grey.
The RF pulse is scaled for a flip angle of 30 degrees, and the gradient is scaled for a slice thickness of 3 mm,
which were the values used in all scans in this study.
The y-gradient is scaled for a slab thickness of 100 mm.}
\label{fig:pulses}
\end{figure}

\begin{figure}[!h]
\centering
%\includegraphics[width=\textwidth]{figures/Fig2inVivoImagesStdMaps.eps}
\caption{In vivo temperature precision. The top row shows the magnitudes of the baseline (second time point) images,
in three planes for the full- and reduced-FOV sequences.
The second row shows corresponding through-time temperature standard deviation maps.
The orange arrows in the coronal reduced-FOV image point to regions of signal loss
where off-resonance caused the brain tissue to fall out of the two-dimensional pulse's passband,
and the blue arrow points to a region of signal loss in the sagittal image.
}
\label{fig:invivo}
\end{figure}

\begin{figure}[!h]
\centering
%\includegraphics[width=\textwidth]{figures/Fig3imgsTempMaps.eps}
\caption{Temperature maps measured in a gel phantom during FUS heating.
The top row shows the magnitudes of the baseline (second time point) images for the full- and reduced-FOV sequences,
in axial and coronal slice orientations.
The reduced-FOV pulse successfully excited the phantom region in both cases, but also excited the outer edge of water bath
due to the narrow 70 mm slab width,
which was prevented from aliasing into the phantom by appropriate adjustment of the phase encode FOV (orange arrows).
The second row shows temperature maps at peak heat, and the third row shows temperature maps at peak heat with windowing
adjusted to highlight background temperature errors.
}
\label{fig:phantomMaps}
\end{figure}

\begin{figure}[!h]
\centering
%\includegraphics[width=\textwidth]{figures/Fig4tempPlots.eps}
\caption{Temperature versus time in the voxels with highest heating,
which were in the same location between the full- and reduced-FOV maps for each slice orientation.
}
\label{fig:phantomPlots}
\end{figure}

\begin{figure}[!h]
\centering
%\includegraphics[width=0.5\textwidth]{figures/Fig5acc2x.eps}
\caption{Head phantom results. The top row shows the magnitudes of the fully-sampled baseline (second time point) images,
for the full-FOV and reduced-FOV sequences.
The second row shows temperature maps reconstructed by the k-space hybrid method
from uniformly 2x-undersampled k-space data from the first time point in the series (immediately after the baseline).
The third row shows through-time temperature standard deviation maps.}
\label{fig:undersampling}
\end{figure}


\end{document}
